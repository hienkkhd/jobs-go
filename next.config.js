/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
    serverActions: true,
    serverComponentsExternalPackages: ["@prisma/client", "bcryptjs"],
  },
  transpilePackages: ["antd-mobile"],
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "themesdesign.in",
      },
      {
        protocol: "https",
        hostname: "www.vietnamworks.com",
      },
    ],
  },
};

module.exports = nextConfig;
