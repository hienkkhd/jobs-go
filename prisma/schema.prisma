// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

enum USER_TYPE {
  USER
  ADMIN
}

enum GENDER {
  MALE
  FEMALE
}

enum JOB_TYPE {
  FREELANCER
  FULLTIME
  PARTTIME
  FEATURED
}

enum COMPANY_EMPLOYEE_SIZE {
  SMALL
  MEDIUM
  BIG
  BIGER
  SUPPERBIG
}

model Account {
  id                String  @id @default(cuid())
  userId            String
  type              String
  provider          String
  providerAccountId String
  refresh_token     String? @db.Text
  access_token      String? @db.Text
  expires_at        Int?
  token_type        String?
  scope             String?
  id_token          String? @db.Text
  session_state     String?

  user User @relation(fields: [userId], references: [id], onDelete: Cascade)

  @@unique([provider, providerAccountId])
}

model Session {
  id           String   @id @default(cuid())
  sessionToken String   @unique
  userId       String
  expires      DateTime
  user         User     @relation(fields: [userId], references: [id], onDelete: Cascade)
}

model User {
  id                       String         @id @default(cuid())
  createdAt                DateTime       @default(now())
  updateAt                 DateTime       @default(now()) @updatedAt
  name                     String?
  password                 String?
  email                    String?        @unique
  emailVerified            DateTime?
  image                    String?
  is_active                Boolean        @default(true)
  phone_number             String?
  user_type                USER_TYPE      @default(USER)
  sms_notification_active  Boolean        @default(true)
  mail_notification_active Boolean        @default(true)
  gender                   GENDER?
  date_of_birth            DateTime?      @db.Date
  accounts                 Account[]
  sessions                 Session[]
  UserProfile              UserProfile?
  JobPost                  JobPost[]
  Job_Activity             Job_Activity[]
  Job_Bookmark             Job_Bookmark[]
  Blog                     Blog[]
}

model UserProfile {
  id            String          @id @default(cuid())
  createdAt     DateTime        @default(now())
  updateAt      DateTime        @default(now()) @updatedAt
  user_id       User            @relation(fields: [userId], references: [id])
  userId        String          @unique
  User_SkillSet User_SkillSet[]
  Experience    Experience[]
  Education     Education[]
}

model User_SkillSet {
  skill_set_id   SkillSet    @relation(fields: [skillSetId], references: [id])
  skillSetId     String
  userprofile_id UserProfile @relation(fields: [userprofileId], references: [id])
  userprofileId  String
  level          Int         @default(1)

  @@unique([skillSetId, userprofileId])
}

model Experience {
  id             String      @id @default(cuid())
  createdAt      DateTime    @default(now())
  updateAt       DateTime    @default(now()) @updatedAt
  userprofile_id UserProfile @relation(fields: [userprofileId], references: [id])
  userprofileId  String
  is_current_job Boolean     @default(false)
  start_date     DateTime    @db.Date
  end_date       DateTime?   @db.Date
  title          String      @db.Char(60)
  description    String      @db.VarChar(4000)
  company_name   String      @db.Char(40)
  location       String      @db.Char(40)
}

model Education {
  id                 String      @id @default(cuid())
  createdAt          DateTime    @default(now())
  updateAt           DateTime    @default(now()) @updatedAt
  userprofile_id     UserProfile @relation(fields: [userprofileId], references: [id])
  userprofileId      String
  certificate_degree String
  major              String
  university_name    String
  starting_date      DateTime    @db.Date
  completion_date    DateTime    @db.Date
  percentage         Int         @default(4)
  gpa                Int
}

model SkillSet {
  id             String           @id @default(cuid())
  name           String           @unique @db.Char(30)
  createdAt      DateTime         @default(now())
  updateAt       DateTime         @default(now()) @updatedAt
  User_SkillSet  User_SkillSet[]
  Job_Post_Skill Job_Post_Skill[]
}

model VerificationToken {
  identifier String
  token      String   @unique
  expires    DateTime

  @@unique([identifier, token])
}

model JobPost {
  id               String           @id @default(cuid())
  createdAt        DateTime         @default(now())
  updateAt         DateTime         @default(now()) @updatedAt
  posted_by_id     User             @relation(fields: [userId], references: [id])
  userId           String
  slug             String
  company_id       Company?         @relation(fields: [companyId], references: [id])
  companyId        String?
  title            String
  description      String           @db.VarChar(10000)
  location         JobLocation?     @relation(fields: [jobLocationId], references: [id])
  jobLocationId    String?
  Job_Post_Skill   Job_Post_Skill[]
  Job_Activity     Job_Activity[]
  jobType          JOB_TYPE         @default(FULLTIME)
  category         JobCategory      @relation(fields: [jobCategoryId], references: [id])
  jobCategoryId    String
  range_experience String?
  range_salary     String?          @default("Negative")
  Job_Bookmark     Job_Bookmark[]
}

model JobCategory {
  id        String    @id @default(cuid())
  createdAt DateTime  @default(now())
  updateAt  DateTime  @default(now()) @updatedAt
  category  String    @unique @db.Char(80)
  JobPost   JobPost[]
}

model Job_Post_Skill {
  job_id      JobPost  @relation(fields: [jobPostId], references: [id])
  skillset_id SkillSet @relation(fields: [skillSetId], references: [id])
  jobPostId   String
  skillSetId  String
  level       Int      @default(1)

  @@unique([skillSetId, jobPostId])
}

model Job_Activity {
  user_id   User    @relation(fields: [userId], references: [id])
  job_id    JobPost @relation(fields: [jobPostId], references: [id])
  userId    String
  jobPostId String

  @@unique([jobPostId, userId])
}

model Job_Bookmark {
  user_id   User    @relation(fields: [userId], references: [id])
  job_id    JobPost @relation(fields: [jobPostId], references: [id])
  userId    String
  jobPostId String

  @@unique([jobPostId, userId])
}

model JobLocation {
  id            String    @id @default(cuid())
  createdAt     DateTime  @default(now())
  updateAt      DateTime  @default(now()) @updatedAt
  location_name String
  JobPost       JobPost[]
}

model Company {
  id                     String                   @id @default(cuid())
  createdAt              DateTime                 @default(now())
  updateAt               DateTime                 @default(now()) @updatedAt
  name                   String                   @unique
  code                   String
  profile_descriptions   String
  establishment_date     DateTime                 @db.Date
  company_website        String?
  conpany_phonenumber    String?
  company_email          String
  company_logo           String?
  companyImage           CompanyImage[]
  JobPost                JobPost[]
  companySize            COMPANY_EMPLOYEE_SIZE?   @default(MEDIUM)
  Company_BusinessStream Company_BusinessStream[]
}

model BusinessStream {
  id                     String                   @id @default(cuid())
  createdAt              DateTime                 @default(now())
  updateAt               DateTime                 @default(now()) @updatedAt
  stream_name_en         String?                  @unique
  stream_name_vi         String?                  @unique
  Company_BusinessStream Company_BusinessStream[]
}

model Company_BusinessStream {
  createdAt        DateTime       @default(now())
  updateAt         DateTime       @default(now()) @updatedAt
  company          Company        @relation(fields: [companyId], references: [id])
  companyId        String
  businessStream   BusinessStream @relation(fields: [businessStreamId], references: [id])
  businessStreamId String

  @@unique([companyId, businessStreamId])
}

model CompanyImage {
  id         String   @id @default(cuid())
  createdAt  DateTime @default(now())
  updateAt   DateTime @default(now()) @updatedAt
  company_id Company  @relation(fields: [companyId], references: [id])
  companyId  String
  image_url  String
}

model Blog {
  id          String      @id @default(cuid())
  createdAt   DateTime    @default(now())
  updateAt    DateTime    @default(now()) @updatedAt
  user_id     User        @relation(fields: [userId], references: [id])
  userId      String
  title       String      @db.VarChar(100)
  description String      @db.VarChar(254)
  content     String
  slug        String?
  Blog_Tags   Blog_Tags[]
}

model Tags {
  id        String      @id @default(cuid())
  createdAt DateTime    @default(now())
  updateAt  DateTime    @default(now()) @updatedAt
  label     String
  code      String      @unique
  Blog_Tags Blog_Tags[]
}

model Blog_Tags {
  createdAt DateTime @default(now())
  updateAt  DateTime @default(now()) @updatedAt
  tag       Tags     @relation(fields: [tagsId], references: [id])
  blog      Blog     @relation(fields: [blogId], references: [id])
  tagsId    String
  blogId    String

  @@unique([tagsId, blogId])
}
