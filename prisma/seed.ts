import { JobPost, PrismaClient, User } from "@prisma/client";
import slugify from "slugify";
const prisma = new PrismaClient();
async function main() {}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
