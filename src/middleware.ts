import { USER_TYPE } from "@prisma/client";
import { withAuth } from "next-auth/middleware";
import { redirect } from "next/navigation";

export default withAuth(
  // `withAuth` augments your `Request` with the user's token.
  function middleware(req) {
    const { token } = req.nextauth;
    const { pathname } = req.nextUrl;
    if (pathname.startsWith("/admin")) {
      redirect("/login");
    }
  },
  {
    callbacks: {
      authorized: ({ token, req }: any) => {
        const { pathname } = req.nextUrl;
        console.log(token?.userType);

        if (
          pathname.startsWith("/admin") &&
          token?.userType !== USER_TYPE.ADMIN
        ) {
          return false;
        }
        return !!token;
      },
    },
  }
);

export const config = { matcher: ["/admin"] };
