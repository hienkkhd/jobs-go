"use client";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import CSIcon from "./components/Icon";

type Props = {};

function NotFound({}: Props) {
  return (
    <div className="flex justify-center items-center bg-violet-500/20 h-[100vh]">
      <div className="container mx-auto">
        <div className="grid items-center justify-center grid-cols-12">
          <div className="col-span-12 lg:col-span-6 lg:col-start-4">
            <div className="text-center">
              <Image
                src="https://themesdesign.in/jobcy-tailwind/layout/assets/images/404.png"
                alt=""
                className="img-fluid"
                width={800}
                height={700}
              />
              <div className="mt-10">
                <h4 className="mt-3 mb-1 uppercase text-22 font-semibold">
                  Sorry, page not found
                </h4>
                <p className="text-gray-500 dark:text-gray-300">
                  It will be as simple as Occidental in fact, it will be
                  Occidental
                </p>
                <div className="mt-8 flex justify-center">
                  <Link
                    className="text-white border-transparent bg-violet-500 py-3 rounded-md flex items-center w-[180px] justify-center gap-2"
                    href="/"
                  >
                    <CSIcon name="mdi-home" /> Back to Home
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NotFound;
