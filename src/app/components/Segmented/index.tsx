"use client";
import { Segmented, SegmentedProps } from "antd";
import React, { ReactNode } from "react";
import "./style.css";

type Props = {} & SegmentedProps;

function CSSegmented({ options, onChange }: Props) {
  return <Segmented size="large" options={options} onChange={onChange} />;
}

export default CSSegmented;
