import Image from "next/image";
import subscribe from "../../../assets/images/subscribe.png";
import React from "react";
import Link from "next/link";
import CSIcon from "../Icon";

type Props = {};

function Footter({}: Props) {
  return (
    <div>
      <section className="py-16 bg-zinc-700 relative overflow-hidden">
        <div className="container px-5 lg:px-24 mx-auto flex justify-between items-center ">
          <div>
            <p className="text-lg font-bold text-white">
              Get New Jobs Notification!
            </p>
            <p className="text-md mt-1 mb-0 text-white/50 dark:text-gray-300">
              Subscribe &amp; get all related jobs notification.
            </p>
          </div>
          <form className="flex rounded-md overflow-hidden border-t border-l border-b border-white border-gray-50/30 w-[36%] justify-between h-[46px] relative z-10">
            <input
              type="text"
              className="bg-transparent pl-2 flex-1 text-white text-md"
              placeholder="Enter your email"
              color="white"
            />
            <p className="text-white px-4 bg-violet-500 flex items-center">
              Subscribe
            </p>
          </form>
          <div className="absolute right-0 -top-6 z-0 ">
            <Image
              src={subscribe}
              alt="Subscribe"
              width={420}
              height={350}
              className="opacity-40"
            />
          </div>
        </div>
      </section>
      <section className="py-16 bg-zinc-800 relative overflow-hidden">
        <div className="grid grid-cols-12 lg:gap-10 container px-5 lg:px-24 mx-auto">
          <div className="col-span-12 xl:col-span-4">
            <div className="mr-12">
              <h4 className="text-white mb-6 text-[23px]">Jobcy</h4>
              <p className="text-white/50 dark:text-gray-300">
                It is a long established fact that a reader will be of a page
                reader will be of at its layout.
              </p>
              <p className="mt-3 text-white dark:text-gray-50">Follow Us on:</p>
              <div className="mt-5">
                <ul className="flex gap-3">
                  <Link
                    href={"/"}
                    className="w-8 h-8 leading-loose text-center text-gray-200 transition-all duration-300 rounded-full cursor-pointer hover:text-gray-50 group-data-[theme-color=violet]:hover:bg-violet-500 group-data-[theme-color=sky]:hover:bg-sky-500 group-data-[theme-color=red]:hover:bg-red-500 group-data-[theme-color=green]:hover:bg-green-500 group-data-[theme-color=pink]:hover:bg-pink-500 group-data-[theme-color=blue]:hover:bg-blue-500 hover:border-transparent"
                  >
                    <CSIcon name="logos:facebook" fontSize={26} />
                  </Link>
                  <Link
                    href={"/"}
                    className="w-8 h-8 leading-loose text-center text-gray-200 transition-all duration-300 rounded-full cursor-pointer hover:text-gray-50 group-data-[theme-color=violet]:hover:bg-violet-500 group-data-[theme-color=sky]:hover:bg-sky-500 group-data-[theme-color=red]:hover:bg-red-500 group-data-[theme-color=green]:hover:bg-green-500 group-data-[theme-color=pink]:hover:bg-pink-500 group-data-[theme-color=blue]:hover:bg-blue-500 hover:border-transparent"
                  >
                    <CSIcon name="skill-icons:instagram" fontSize={26} />
                  </Link>
                  <Link
                    href={"/"}
                    className="w-8 h-8 leading-loose text-center text-gray-200 transition-all duration-300 rounded-full cursor-pointer hover:text-gray-50 group-data-[theme-color=violet]:hover:bg-violet-500 group-data-[theme-color=sky]:hover:bg-sky-500 group-data-[theme-color=red]:hover:bg-red-500 group-data-[theme-color=green]:hover:bg-green-500 group-data-[theme-color=pink]:hover:bg-pink-500 group-data-[theme-color=blue]:hover:bg-blue-500 hover:border-transparent"
                  >
                    <CSIcon name="devicon:google" fontSize={26} />
                  </Link>
                  <Link
                    href={"/"}
                    className="w-8 h-8 leading-loose text-center text-gray-200 transition-all duration-300 rounded-full cursor-pointer hover:text-gray-50 group-data-[theme-color=violet]:hover:bg-violet-500 group-data-[theme-color=sky]:hover:bg-sky-500 group-data-[theme-color=red]:hover:bg-red-500 group-data-[theme-color=green]:hover:bg-green-500 group-data-[theme-color=pink]:hover:bg-pink-500 group-data-[theme-color=blue]:hover:bg-blue-500 hover:border-transparent"
                  >
                    <CSIcon name="logos:twitter" fontSize={26} />
                  </Link>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-span-12 mt-8 md:col-span-6 xl:col-span-2 md:mt-0">
            <p className="mb-6 text-white text-16">Company</p>
            <ul className="space-y-4">
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="about.html">
                  <i className="mdi mdi-chevron-right"></i> About Us
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="contact.html">
                  <i className="mdi mdi-chevron-right"></i> Contact Us
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="services.html">
                  <i className="mdi mdi-chevron-right"></i> Services
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="blog.html">
                  <i className="mdi mdi-chevron-right"></i> Blog
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="team.html">
                  <i className="mdi mdi-chevron-right"></i> Team
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="pricing.html">
                  <i className="mdi mdi-chevron-right"></i> Pricing
                </Link>
              </li>
            </ul>
          </div>
          <div className="col-span-12 mt-8 md:col-span-6 xl:col-span-2 md:mt-0">
            <p className="mb-6 text-white text-16">For Jobs</p>
            <ul className="space-y-4">
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="job-categories.html">
                  <i className="mdi mdi-chevron-right"></i> Browser Categories
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="job-list.html">
                  <i className="mdi mdi-chevron-right"></i> Browser Jobs
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="job-details.html">
                  <i className="mdi mdi-chevron-right"></i> Job Details
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="bookmark-jobs.html">
                  <i className="mdi mdi-chevron-right"></i> Bookmark Jobs
                </Link>
              </li>
            </ul>
          </div>
          <div className="col-span-12 mt-8 md:col-span-6 xl:col-span-2 md:mt-0">
            <p className="mb-6 text-white text-16">For Candidates</p>
            <ul className="space-y-4">
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="candidate-list.html">
                  <i className="mdi mdi-chevron-right"></i> Candidate List
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="candidate-grid.html">
                  <i className="mdi mdi-chevron-right"></i> Candidate Grid
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="candidate-details.html">
                  <i className="mdi mdi-chevron-right"></i> Candidate Details
                </Link>
              </li>
            </ul>
          </div>
          <div className="col-span-12 mt-8 md:col-span-6 xl:col-span-2 md:mt-0">
            <p className="mb-6 text-white text-16">Support</p>
            <ul className="space-y-4">
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="contact.html">
                  <i className="mdi mdi-chevron-right"></i> Help Center
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="faqs.html">
                  <i className="mdi mdi-chevron-right"></i> FAQ{"'"}S
                </Link>
              </li>
              <li className="text-sm transition-all duration-500 text-white/50 hover:text-gray-50 hover:text-[15px] dark:text-gray-300 dark:hover:text-gray-50">
                <Link href="privacy-policy.html">
                  <i className="mdi mdi-chevron-right"></i> Privacy Policy
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Footter;
