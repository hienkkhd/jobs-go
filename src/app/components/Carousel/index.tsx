"use client";
import { Carousel, CarouselProps } from "antd";
import React, { ReactElement } from "react";
import "./style.css";
type Props = {
  children: ReactElement[];
} & CarouselProps;

function CSCarousel({ children, ...rest }: Props) {
  return (
    <Carousel autoplay {...rest}>
      {children}
    </Carousel>
  );
}

export default CSCarousel;
