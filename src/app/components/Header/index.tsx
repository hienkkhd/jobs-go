"use client";
import { fetcher } from "@/lib/fetcher";
import { Icon } from "@iconify/react";
import {
  Avatar,
  Badge,
  Button,
  Menu,
  MenuProps,
  Popover,
  Skeleton,
} from "antd";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import useSWR from "swr";
import { LoginButton, LogoutButton } from "../auth";
import styles from "./styles.module.css";
type Props = {};

function Header({}: Props) {
  const { data, isLoading, error } = useSWR("/api/user", fetcher);
  const pathName = usePathname();
  const [scrollTop, setScrollTop] = useState(0);
  const handleScroll = (event: any) => {
    setScrollTop(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    if (typeof window !== "undefined") {
      window.scroll({ top: window.scrollY - 1 });
    }
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  const items: MenuProps["items"] = [
    {
      label: (
        <Link
          href={"/"}
          className="text-[15px] px-4 text-gray-500 font-semibold hover:text-blue-400"
        >
          Home
        </Link>
      ),
      key: "",
    },
    {
      label: (
        <Link
          href={"/jobs"}
          className="text-[15px] px-4 text-gray-500 font-semibold hover:text-blue-400"
        >
          Jobs
        </Link>
      ),
      key: "jobs",
    },
    {
      label: (
        <Link
          href={"/company"}
          className="text-[15px] px-4 text-gray-500 font-semibold hover:text-blue-400"
        >
          Company
        </Link>
      ),
      key: "company",
      children: [
        {
          label: (
            <Link
              href={"/about"}
              className="mt-1 transition ease-in-out duration-300 text-[15px] px-4 text-gray-500 font-semibold hover:translate-x-2 w-100"
            >
              About us
            </Link>
          ),
          key: "about-us",
        },
        {
          label: (
            <Link
              href={"/services"}
              className="mt-1 transition ease-in-out duration-300 text-[15px] px-4 text-gray-500 font-semibold hover:translate-x-2 w-100"
            >
              Services
            </Link>
          ),
          key: "services",
        },
        {
          label: (
            <Link
              href={"/team"}
              className="mt-1 transition ease-in-out duration-300 text-[15px] px-4 text-gray-500 font-semibold hover:translate-x-2 w-100"
            >
              Team
            </Link>
          ),
          key: "team",
        },
        {
          label: (
            <Link
              href={"/pricing"}
              className="mt-1 transition ease-in-out duration-300 text-[15px] px-4 text-gray-500 font-semibold hover:translate-x-2 w-100"
            >
              Pricing
            </Link>
          ),
          key: "pricing",
        },
        {
          label: (
            <Link
              href={"/privacy"}
              className="mt-1 transition ease-in-out duration-300 text-[15px] px-4 text-gray-500 font-semibold hover:translate-x-2 w-100"
            >
              Privacy and Policy
            </Link>
          ),
          key: "privacy",
        },
        {
          label: (
            <Link
              href={"/faqs"}
              className="mt-1 transition ease-in-out duration-300 text-[15px] px-4 text-gray-500 font-semibold hover:translate-x-2 w-100"
            >
              FAQS
            </Link>
          ),
          key: "faqs",
        },
      ],
    },
    {
      label: (
        <Link
          href={"/blogs"}
          className=" text-[15px] px-4 text-gray-500 font-semibold hover:text-blue-400"
        >
          Blogs
        </Link>
      ),
      key: "blogs",
    },
    {
      label: (
        <Link
          href={"/contact"}
          className=" text-[15px] px-4 text-gray-500 font-semibold hover:text-blue-400"
        >
          Contact
        </Link>
      ),
      key: "contact",
    },
  ];
  const itemProfile: MenuProps["items"] = [
    {
      key: "1",
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.antgroup.com"
        >
          1st menu item
        </a>
      ),
    },
    {
      key: "2",
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.aliyun.com"
        >
          2nd menu item
        </a>
      ),
    },
    {
      key: "3",
      label: (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.luohanacademy.com"
        >
          3rd menu item
        </a>
      ),
    },
  ];
  return (
    <div
      className={`transition-all duration-500 ease-in-out fixed top-0 w-[100%] z-50 ${
        scrollTop > 100 && "lg:-translate-y-12"
      }`}
    >
      <div className={`${styles.headerContainer} hidden lg:block `}>
        <div className="container-fluid px-5 lg:px-24 mx-auto flex py-3 gap-4 items-center text-gray-800">
          <div className="mb-0 text-gray-800 text-13 dark:text-gray-5 flex items-center gap-1">
            <Icon icon="gridicons:location" />
            Your Location: <p className="font-medium"> New Caledonia</p>
          </div>
          <Icon icon="uil:whatsapp" />
          <Icon icon="uil:facebook-messenger-alt" />
          <Icon icon="uil:instagram" />
          <Icon icon="circum:mail" />
          <Icon icon="uil:twitter-alt" />
        </div>
      </div>
      <div
        className={` container-fluid px-5 lg:px-24 mx-auto flex items-center justify-between bg-white shadow-md`}
      >
        <Image src={"/logo-dark.png"} alt="Logo" width={102} height={22} />
        <Menu
          defaultSelectedKeys={[pathName.replace("/", "")]}
          style={{
            minWidth: 0,
            flex: "auto",
            justifyContent: "center",
            height: 72,
            alignItems: "center",
          }}
          mode="horizontal"
          items={items}
        />
        <div className="flex items-center gap-2">
          {isLoading ? (
            <div>
              <Skeleton.Input active size="small" />
            </div>
          ) : !!data?.user ? (
            <div className="flex items-center">
              <Badge count={1} size="small" style={{ marginRight: 8 }}>
                <Icon icon="iconamoon:notification-fill" fontSize={24} />
              </Badge>
              <Popover
                content={
                  <div className="flex-col rounded-none px-4">
                    <div className="py-1 text-[16px] font-semibold">
                      <Link className="text-gray-500" href={"/profile"}>
                        Manage Jobs
                      </Link>
                    </div>
                    <div className="py-1 text-[16px] font-semibold">
                      <Link className="text-gray-500" href={"/profile"}>
                        Bookmark Jobs
                      </Link>
                    </div>
                    <div className="py-1 text-[16px] font-semibold">
                      <Link className="text-gray-500" href={"/profile"}>
                        My Profile
                      </Link>
                    </div>
                    <div className="py-1 text-[16px] font-semibold">
                      <div className="text-gray-500">
                        <LogoutButton />
                      </div>
                    </div>
                  </div>
                }
                placement="bottom"
              >
                <Button
                  className="flex items-center"
                  size="large"
                  icon={
                    <Avatar
                      style={{
                        marginTop: -4,
                        verticalAlign: "middle",
                      }}
                      size={"default"}
                      src={data?.user?.image}
                    >
                      {data?.user?.image ||
                        data?.user?.name?.charAt(0).toUpperCase() ||
                        data?.user?.email?.charAt(0).toUpperCase()}
                    </Avatar>
                  }
                  type="text"
                >
                  <p className="hidden md:block">
                    {data?.user?.name || data?.user?.email}
                  </p>
                </Button>
              </Popover>
            </div>
          ) : (
            <LoginButton />
          )}
        </div>
      </div>
    </div>
  );
}

export default Header;
