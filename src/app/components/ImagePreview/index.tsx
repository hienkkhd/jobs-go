"use client";
import { Image } from "antd";
import React, { ReactElement } from "react";

type Props = {
  srcList: { src: string; alt?: string }[];
};

async function ImagePreview({ srcList }: Props) {
  return (
    <Image.PreviewGroup>
      <div className="grid grid-cols-12 gap-y-2 lg:gap-x-4">
        {srcList &&
          srcList.map((item, index) => {
            return (
              <div
                key={index}
                className={`${
                  index % 3 === 2
                    ? "col-span-12 max-h-[510px]"
                    : "col-span-12 lg:col-span-6 max-h-[255px]"
                } rounded-md overflow-hidden `}
              >
                <Image
                  width={"100%"}
                  height={"100%"}
                  src={item.src}
                  alt={item.alt}
                  className="rounded-md overflow-hidden object-cover"
                />
              </div>
            );
          })}
      </div>
    </Image.PreviewGroup>
  );
}

export default ImagePreview;
