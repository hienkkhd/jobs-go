"use client";
import { Select } from "antd";
import React from "react";

type Props = {};

function CSSelect({}: Props) {
  return (
    <Select
      showSearch
      style={{
        width: "100%",
        border: "none",
      }}
      placeholder={<p className="text-sm mt-1 text-gray-400">Select location</p>}
      optionFilterProp="children"
      bordered={false}
      filterOption={(input, option) =>
        (option?.title.toLowerCase() ?? "").includes(input.toLowerCase())
      }
      options={[
        {
          value: "1",
          label: <p className="text-md font-semibold text-gray-800">Vietnam</p>,
          title: "Vietnam",
        },
        {
          value: "2",
          label: <p className="text-md font-semibold text-gray-800">Korea</p>,
          title: "korea",
        },
        {
          value: "3",
          label: <p className="text-md font-semibold text-gray-800">Japan</p>,
          title: "japan",
        },
      ]}
    />
  );
}

export default CSSelect;
