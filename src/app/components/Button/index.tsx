"use client";

import { signIn } from "next-auth/react";
import CSIcon from "../Icon";
type Props = {
  callbackUrl?: string;
};
export function LoginGoogleButton({ callbackUrl = "/" }: Props) {
  const signInGoogle = async () => {
    signIn("google", { callbackUrl, redirect: true })
      .then(() => {
        console.log(123);
      })
      .catch();
  };
  return (
    <button
      onClick={signInGoogle}
      className="px-5 py-2 border-[1px] border-white rounded-md transition ease-in-out duration-300 hover:bg-white/80 hover:-translate-y-1"
    >
      <CSIcon name="flat-color-icons:google" fontSize={30} />
    </button>
  );
}
