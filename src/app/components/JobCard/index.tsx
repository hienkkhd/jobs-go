import wordPress from "@/assets/images/word-press.png";
import { JobPost } from "@prisma/client";
import Image from "next/image";
import CSIcon from "../Icon";

export default function JobCard({
  title,
  jobType,
  range_salary,
}: Pick<JobPost, "title" | "jobType" | "range_salary">) {
  return (
    <div className="transition-all ease-in-out duration-500 hover:-translate-y-2 hover:border-violet-500 relative overflow-hidden border border-red-100 shadow-sm mt-4 rounded-md">
      <div className="bg-white p-5 pb-8 flex items-center justify-evenly gap-1">
        <Image src={wordPress} width={55} height={55} alt="Wordpress" />
        <div className="w-[42%]">
          <h3 className="text-lg font-semibold text-gray-700 line-clamp-1">
            {title}
          </h3>
          <p className="pt-1">Web Technology pvt.Ltd</p>
        </div>
        <div className="flex gap-5 items-center">
          <div className="flex gap-1 items-center text-md">
            <CSIcon name="uil:location-point" color="blue" />
            Hà nội
          </div>
          <p className="text-blue-500">{range_salary || "$ Negative"}</p>
        </div>
        <div className="flex flex-col gap-2">
          <div className="bg-[#b3b5c1]/20 py-0.5 px-2 text-[#048565] text-xs font-medium rounded-xs">
            {jobType}
          </div>
        </div>
      </div>
      <div className="p-3 bg-gray-50 dark:bg-neutral-700 grid grid-cols-12 items-center">
        <div className="flex gap-2 col-span-4 items-center">
          <h4 className="text-md font-medium text-gray-500">Experience</h4>
          <p>: 1 - 2 years</p>
        </div>
        <div className="flex gap-2 col-span-6 items-center">
          <h4 className="text-md font-medium text-gray-500">Notes </h4>
          <p>: languages only differ in their grammar.</p>
        </div>
        <div className="flex gap-2 col-span-2 text-violet-500">
          <button className="px-2">Apply Now {">>"}</button>
        </div>
      </div>
    </div>
  );
}
