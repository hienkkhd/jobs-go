"use client";
import { Icon } from "@iconify/react";
import React from "react";

type Props = {
  name: string;
  color?: string;
  fontSize?: string | number;
  className?: string;
};

function CSIcon({ name, color, fontSize, className }: Props) {
  return (
    <Icon
      icon={name}
      color={color}
      fontSize={fontSize}
      className={className}
    ></Icon>
  );
}

export default CSIcon;
