"use client";
import { Pagination, PaginationProps } from "antd";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import queryString from "query-string";
import React from "react";

type Props = PaginationProps;

function CSPagination(props: Props) {
  const searchParams = useSearchParams();
  const page = Number(searchParams.get("page")) || 1;
  const limit = Number(searchParams.get("limit")) || 10;
  const router = useRouter();
  const pathName = usePathname();
  const queryObj = queryString.parse(searchParams.toString());
  const handleOnChange = ({ page, limit }: { page: number; limit: number }) => {
    const query = { ...queryObj, page, limit };
    router.push(`${pathName}?${queryString.stringify(query)}`);
  };
  return (
    <Pagination
      {...props}
      pageSize={limit}
      current={page}
      total={props.total || limit}
      onChange={(page, limit) => {
        handleOnChange({ page, limit });
      }}
    />
  );
}

export default CSPagination;
