import { Suspense } from "react";
import bgHead from "@/assets/images/shape.png";
import Image from "next/image";
import CompanyList from "./components/CompanyList";
import Loading from "../loading";

type Props = {
  params: { slug: string };
  searchParams?: { [key: string]: string | string[] | undefined };
};

export default function Page({ params, searchParams }: Props) {
  return (
    <main>
      <section className="w-full bg-violet-500 pt-28 lg:pt-[300px] lg:py-[160px] relative overflow-hidden flex items-center justify-center flex-col">
        <Image
          src={bgHead}
          width={3500}
          height={40}
          alt="BG"
          className="absolute bottom-0"
        />
        <h1 className="text-white text-[26px] font-bold">Company list</h1>
        <div className="flex gap-3 mt-2 items-center">
          <p className="text-white text-sm font-normal uppercase">Home</p>
          <p className="text-white text-sm font-normal uppercase">{">"}</p>
          <p className="text-white text-sm font-normal uppercase">
            Company list
          </p>
        </div>
      </section>
      <section className="mt-5">
        <div className="container mx-auto">
          <Suspense fallback={<Loading />}>
            <CompanyList params={params} searchParams={searchParams} />
          </Suspense>
        </div>
      </section>
    </main>
  );
}
