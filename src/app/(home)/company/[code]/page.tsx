import CSIcon from "@/app/components/Icon";
import ImagePreview from "@/app/components/ImagePreview";
import JobCard from "@/app/components/JobCard";
import bgHead from "@/assets/images/shape.png";
import { prisma } from "@/lib/prisma";
import { Image as ImageAntd } from "antd";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { notFound } from "next/navigation";
import { Suspense } from "react";

type Props = {
  params: { code: string };
  searchParams?: { [key: string]: string | string[] | undefined };
};

async function CompanyDetail({ params }: Props) {
  const company = await prisma.company.findFirst({
    where: {
      code: params.code,
    },
  });
  if (!company) {
    notFound();
  }

  return (
    <main>
      <section className="w-full bg-violet-500 pt-28 lg:pt-[300px] lg:py-[160px] relative overflow-hidden flex items-center justify-center flex-col">
        <Image
          src={bgHead}
          width={3500}
          height={40}
          alt="BG"
          className="absolute bottom-0"
        />
        <h1 className="text-white text-[26px] font-bold">Company Detail</h1>
        <div className="flex gap-3 mt-2 items-center">
          <p className="text-white text-sm font-normal uppercase">Home</p>
          <p className="text-white text-sm font-normal uppercase">{">"}</p>
          <p className="text-white text-sm font-normal uppercase">
            Company Detail
          </p>
        </div>
      </section>
      <section>
        <div className="container mx-auto py-5">
          <div className="grid grid-cols-12 gap-y-10 lg:gap-10">
            <div className="col-span-12 lg:col-span-4 rounded-sm overflow-hidden">
              <div className="border border-b-0 p-5">
                <div className="flex items-center flex-col">
                  <div className="w-[70px] h-[70px] rounded-full shadow-md overflow-hidden flex justify-center items-center">
                    <Image
                      src={company.company_logo?.split(" ")[0] as string}
                      alt={company.name}
                      width={60}
                      height={60}
                    />
                  </div>
                  <h3 className="mt-4 font-semibold line-clamp-2 text-gray-800">
                    {company.name}
                  </h3>
                  <h4 className="text-[15px] mb-4 text-gray-500">
                    Since{" "}
                    {moment(company.establishment_date).format("MMM YYYY")}
                  </h4>
                  <ul className="flex flex-wrap justify-center gap-4">
                    <li className="h-10 w-10 text-center bg-gray-50 shadow-sm rounded-full hover:bg-violet-500 transition-colors duration-500 ease-in-out cursor-pointer  hover:text-white text-gray-500">
                      <Link
                        href="/"
                        className="flex justify-center items-center h-full"
                      >
                        <CSIcon name="uil-twitter-alt" className="text-lg" />
                      </Link>
                    </li>
                    <li className="h-10 w-10 text-center bg-gray-50 shadow-sm rounded-full hover:bg-violet-500 transition-colors duration-500 ease-in-out cursor-pointer  hover:text-white text-gray-500">
                      <Link
                        href="/"
                        className="flex justify-center items-center h-full"
                      >
                        <CSIcon name="uil-whatsapp" className="text-lg" />
                      </Link>
                    </li>
                    <li className="h-10 w-10 text-center bg-gray-50 shadow-sm rounded-full hover:bg-violet-500 transition-colors duration-500 ease-in-out cursor-pointer  hover:text-white text-gray-500">
                      <Link
                        href="/"
                        className="flex justify-center items-center h-full"
                      >
                        <CSIcon name="uil-phone-alt" className="text-lg" />
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="border border-b-0 p-5">
                <h3 className="font-semibold text-gray-800 mb-5">
                  Profile Overview
                </h3>
                <div className="mb-4">
                  <h4 className="text-gray-900 font-medium">Owner Name</h4>
                  <h5 className="text-gray-500">Nguyen dinh hien</h5>
                </div>
                <div className="flex flex-wrap mb-4">
                  <h4 className="text-gray-900 font-medium w-[118px]">
                    Employees
                  </h4>
                  <h5 className="text-gray-500">1500-1850</h5>
                </div>
                <div className="flex flex-wrap mb-4">
                  <h4 className="text-gray-900 font-medium w-[118px]">
                    Location
                  </h4>
                  <h5 className="text-gray-500">New York</h5>
                </div>
                <div className="flex flex-wrap mb-4">
                  <h4 className="text-gray-900 font-medium w-[118px]">
                    Website
                  </h4>
                  <h5 className="text-gray-500">
                    www.Jobcytecnologypvt.ltd.com
                  </h5>
                </div>
                <div className="flex flex-wrap mb-4">
                  <h4 className="text-gray-900 font-medium w-[118px]">
                    Established
                  </h4>
                  <h5 className="text-gray-500">
                    {moment(company.establishment_date).format("MMM DD YYYY")}
                  </h5>
                </div>
                <div className="flex flex-wrap mb-8">
                  <h4 className="text-gray-900 font-medium w-[118px]">Views</h4>
                  <h5 className="text-gray-500">2574</h5>
                </div>
                <button className="w-full h-[44px] flex justify-center items-center bg-red-600 rounded-[0.3em] text-white gap-2 ">
                  <CSIcon name="uil-phone" />
                  Contact
                </button>
              </div>
              <div className="border border-b-0 p-5">
                <h3 className="font-semibold text-gray-800 mb-5">
                  Working Days
                </h3>
                <div className="flex flex-wrap mb-3 justify-between">
                  <h4 className="text-gray-900 font-normal ">Monday</h4>
                  <h5 className="text-gray-500 text-sm">9AM-5PM</h5>
                </div>
                <div className="flex flex-wrap mb-3 justify-between">
                  <h4 className="text-gray-900 font-normal ">Tuesday</h4>
                  <h5 className="text-gray-500 text-sm">9AM-5PM</h5>
                </div>
                <div className="flex flex-wrap mb-3 justify-between">
                  <h4 className="text-gray-900 font-normal ">Wednesday</h4>
                  <h5 className="text-gray-500 text-sm">9AM-5PM</h5>
                </div>
                <div className="flex flex-wrap mb-3 justify-between">
                  <h4 className="text-gray-900 font-normal ">Thursday</h4>
                  <h5 className="text-gray-500 text-sm">9AM-5PM</h5>
                </div>
                <div className="flex flex-wrap mb-3 justify-between">
                  <h4 className="text-gray-900 font-normal ">Friday</h4>
                  <h5 className="text-gray-500 text-sm">9AM-5PM</h5>
                </div>
                <div className="flex flex-wrap mb-3 justify-between">
                  <h4 className="text-gray-900 font-normal ">Saturday</h4>
                  <h5 className="text-gray-500 text-sm">9AM-5PM</h5>
                </div>
                <div className="flex flex-wrap mb-3 justify-between">
                  <h4 className="text-gray-900 font-normal ">Sunday</h4>
                  <h5 className="text-sm text-red-500">Close</h5>
                </div>
              </div>
              <div className="border p-5">
                <h3 className="font-semibold text-gray-800 mb-5">
                  Company Location
                </h3>
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29797.950533513675!2d105.76854138563237!3d21.002903920366123!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345a55e1834bed%3A0xc31ac4aae53c35d!2zxJBDVDA4LCBN4buFIFRyw6wsIEjDoCBO4buZaSwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1687919094427!5m2!1sen!2s"
                  width="100%"
                  height="250px"
                  style={{
                    border: 0,
                  }}
                  allowFullScreen={true}
                  loading="lazy"
                  referrerPolicy="no-referrer-when-downgrade"
                ></iframe>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-8 border border-1 rounded-sm p-6">
              <h3 className="font-semibold text-gray-800 mb-5">
                About Company
              </h3>
              <div className="text-gray-500 text-[15px]">
                <p className="mb-4">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <p className="mb-4">
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities Appropriately communicate one-to-one
                  technology.
                </p>
                <p className="mb-2">
                  Exercitation photo booth stumptown tote bag Banksy, elit small
                  batch freegan sed. Craft beer elit seitan exercitation, photo
                  booth et 8-bit kale chips proident chillwave deep v laborum.
                  Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami
                  readymade swag.
                </p>
              </div>
              <div className="mt-5">
                <h3 className="font-semibold text-gray-800 mb-3">Gallery</h3>
                <ImagePreview
                  srcList={[
                    {
                      src: "https://themesdesign.in/jobcy-tailwind/layout/assets/images/blog/img-01.jpg",
                      alt: "Image1",
                    },
                    {
                      src: "https://themesdesign.in/jobcy-tailwind/layout/assets/images/blog/img-03.jpg",
                      alt: "Image2",
                    },
                    {
                      src: "https://themesdesign.in/jobcy-tailwind/layout/assets/images/blog/img-12.jpg",
                      alt: "Image3",
                    },
                  ]}
                />
              </div>
              <div className="mt-5">
                <h3 className="font-semibold text-gray-800 mb-3">
                  Current Opening
                </h3>
                <div>
                  <JobCard
                    title="HTML Developer"
                    jobType="FULLTIME"
                    range_salary={"$ 1500 - 1700"}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}

export default CompanyDetail;
