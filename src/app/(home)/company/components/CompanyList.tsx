import CSPagination from "@/app/components/Pagination";
import { prisma } from "@/lib/prisma";
import CompanyCard from "./CompanyCard";

type Props = {
  params: { slug: string };
  searchParams?: { [key: string]: string | string[] | undefined };
};
async function CompanyList({ params, searchParams }: Props) {
  const page = Number(searchParams?.page) || 1;
  const limit = Number(searchParams?.limit) || 10;
  const data = await prisma.company.findMany({
    include: {
      _count: {
        select: {
          JobPost: true,
        },
      },
      Company_BusinessStream: {
        select: {
          businessStream: {
            select: {
              stream_name_en: true,
              stream_name_vi: true,
            },
          },
        },
      },
      companyImage: {
        select: {
          image_url: true,
        },
      },
    },
    skip: (page - 1) * limit,
    take: limit,
  });
  const total = await prisma.company.count();

  return (
    <div className="grid grid-cols-12 gap-y-8 md:gap-6 lg:gap-8 py-10">
      {data?.map((item, index: number) => {
        return (
          <CompanyCard
            key={item.id}
            company_logo={item.company_logo}
            name={item.name}
            jobCount={item._count.JobPost}
            companyImage={item.companyImage[0]?.image_url || null}
            businessStream={item.Company_BusinessStream}
            code={item.code}
          />
        );
      })}
      <div className="col-span-12 flex justify-center items-center">
        <CSPagination total={Number(total) || 10} />
      </div>
    </div>
  );
}

export default CompanyList;
