import CSIcon from "@/app/components/Icon";
import {
  BusinessStream,
  Company,
  Company_BusinessStream,
} from "@prisma/client";
import Image from "next/image";
import Link from "next/link";
import React from "react";

type Props = Pick<Company, "company_logo" | "name" | "code"> & {
  jobCount: number;
  companyImage: string | null;
  businessStream: any[];
};
function CompanyCard({
  company_logo,
  name,
  jobCount,
  companyImage,
  businessStream,
  code,
}: Props) {
  let stream = "Other";

  if (businessStream.length > 0) {
    stream = businessStream
      .map((item) => {
        return item.businessStream.stream_name_vi;
      })
      .join("");
  }
  return (
    <div className="col-span-12 md:col-span-6 lg:col-span-4">
      <div className="relative border rounded-md border-gray-100/50 dark:border-neutral-600 shadow-md p-3">
        <div className="relative h-[117px]">
          <Image
            src={
              `https://www.vietnamworks.com/${
                companyImage?.split(" ")[0]
              }` as string
            }
            alt={`company_${name}`}
            fill
            style={{
              objectFit: "cover",
              borderRadius: "0.5em",
            }}
          />
          <div className="absolute bg-white w-[88px] h-[88px] -bottom-[44px] left-5 rounded-md flex items-center p-2 shadow-md">
            <Image
              src={company_logo?.split(" ")[0] as string}
              width={88}
              height={50}
              alt="logo"
            />
          </div>
          <div className="absolute px-2 text-white bg-violet-500 top-3 right-0 ">
            <span className="relative text-xs font-medium uppercase">
              4.9 <i className="mdi mdi-star-outline"></i>
            </span>
            <div className="after:contant[] after:border-r-[13px] after:absolute after:border-b-[13px] after:-left-[12px] after:top-0 after:border-t-[13px] after:border-t-transparent after:border-violet-500 after:border-b-transparent "></div>
          </div>
        </div>
        <div className="mt-11 p-4">
          <Link href={`/company/${code}`}>
            <h6 className=" text-[16px] font-bold text-gray-900 dark:text-white line-clamp-1 capitalize">
              {name}
            </h6>
          </Link>
          <div className="flex items-center gap-1 py">
            <CSIcon name="bi:folder" className="text-gray-500" fontSize={18} />
            <p className=" text-gray-500 dark:text-gray-300 text-md">
              {stream}
            </p>
          </div>
        </div>
        <div className="px-4 pb-2">
          <Link
            href={`/company/${code}`}
            className="text-white border-transparent btn bg-violet-500 px-5 py-3 rounded-md"
          >
            {jobCount} Opening Jobs
          </Link>
        </div>
      </div>
    </div>
  );
}

export default CompanyCard;
