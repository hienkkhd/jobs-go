import bgHead from "@/assets/images/shape.png";
import Image from "next/image";
import avatar from "@/assets/images/avatar.jpg";
import CSIcon from "@/app/components/Icon";
type Props = {};

export default function Page({}: Props) {
  return (
    <main>
      <section className="w-full bg-violet-500 pt-28 lg:pt-[300px] lg:py-[160px] relative overflow-hidden flex items-center justify-center flex-col">
        <Image
          src={bgHead}
          width={3500}
          height={40}
          alt="BG"
          className="absolute bottom-0"
        />
        <h1 className="text-white text-[26px] font-bold">My Profile</h1>
        <div className="flex gap-3 mt-2 items-center">
          <p className="text-white text-sm font-normal uppercase">Home</p>
          <p className="text-white text-sm font-normal uppercase">{">"}</p>
          <p className="text-white text-sm font-normal uppercase">My Profile</p>
        </div>
      </section>
      <section className="mt-5 pb-5">
        <div className="container mx-auto">
          <div className="grid grid-cols-12 gap-y-10 lg:gap-10">
            <div className="col-span-12 lg:col-span-4">
              <div className="border rounded border-gray-100 ">
                <div className="p-5 border-b-[1px]">
                  <div className="flex justify-center items-center w-full flex-col">
                    <Image
                      alt="Avatar"
                      src={avatar}
                      width={80}
                      height={80}
                      className="rounded-full p-1 border-2 border-gray-200/50"
                    />
                    <h3 className="font-medium text-lg mt-3">Jansh Dickens</h3>
                    <p className="text-gray-500 text-sm">Developer</p>
                  </div>
                  <div className="flex justify-center items-center gap-2 text-violet-500 mt-4">
                    <div className="p-3 bg-violet-500/20 rounded-md">
                      <CSIcon name="uil-facebook-f"></CSIcon>
                    </div>
                    <div className="p-3 bg-violet-500/20 rounded-md">
                      <CSIcon name="uil-twitter-alt"></CSIcon>
                    </div>
                    <div className="p-3 bg-violet-500/20 rounded-md">
                      <CSIcon name="uil-whatsapp"></CSIcon>
                    </div>
                    <div className="p-3 bg-violet-500/20 rounded-md">
                      <CSIcon name="uil-phone-alt"></CSIcon>
                    </div>
                  </div>
                </div>
                <div className="p-5 border-b-[1px]">
                  <h3 className="font-semibold text-[17px] mb-4">Documents</h3>
                  <div className="flex items-center mt-2">
                    <div className="pr-3">
                      <CSIcon
                        name="uil-file"
                        className="text-2xl text-gray-500"
                      />
                    </div>
                    <div className="flex-grow">
                      <h4 className="font-medium">Resume.pdf</h4>
                      <p className="text-gray-500 text-sm">125 Mb</p>
                    </div>
                    <div className="p-2 pl-0 cursor-pointer">
                      <CSIcon
                        name="uil-import"
                        className="text-2xl text-gray-500"
                      />
                    </div>
                  </div>
                  <div className="flex items-center mt-2">
                    <div className="pr-3">
                      <CSIcon
                        name="uil-file"
                        className="text-2xl text-gray-500"
                      />
                    </div>
                    <div className="flex-grow">
                      <h4 className="font-medium">Cover-letter.pdf</h4>
                      <p className="text-gray-500 text-sm">125 Mb</p>
                    </div>
                    <div className="p-2 pl-0 cursor-pointer">
                      <CSIcon
                        name="uil-import"
                        className="text-2xl text-gray-500"
                      />
                    </div>
                  </div>
                </div>
                <div className="p-5 border-b-[1px]">
                  <h3 className="font-semibold text-[17px] mb-4">Contact</h3>
                  <div className="flex items-center flex-wrap mt-2">
                    <h3 className="text-md font-medium w-32 text-[16px]">
                      Email
                    </h3>
                    <h4 className="text-gray-500 text-[15px]">
                      hienkkhd@gmail.com
                    </h4>
                  </div>
                  <div className="flex items-center flex-wrap mt-2">
                    <h3 className="text-md font-medium w-32 text-[16px]">
                      Phone number
                    </h3>
                    <h4 className="text-gray-500 text-[15px]">0123456789</h4>
                  </div>
                  <div className="flex items-center flex-wrap mt-2">
                    <h3 className="text-md font-medium w-32 text-[16px]">
                      Location
                    </h3>
                    <h4 className="text-gray-500 text-[15px]">Ha Noi</h4>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-8 rounded">
              <div className="border border-gray-100">
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}
