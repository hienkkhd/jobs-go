import Image from "next/image";
import bgHead from "@/assets/images/shape.png";
import contactImg from "@/assets/images/contact.png";
import CSIcon from "@/app/components/Icon";

type Props = {};

export default function Contact({}: Props) {
  return (
    <main>
      <section className="w-full bg-violet-500 pt-28 lg:pt-[300px] lg:py-[160px] relative overflow-hidden flex items-center justify-center flex-col">
        <Image
          src={bgHead}
          width={3500}
          height={40}
          alt="BG"
          className="absolute bottom-0"
        />
        <h1 className="text-white text-[26px] font-bold">Contact</h1>
        <div className="flex gap-3 mt-2 items-center">
          <p className="text-white text-sm font-normal uppercase">Home</p>
          <p className="text-white text-sm font-normal uppercase">{">"}</p>
          <p className="text-white text-sm font-normal uppercase">Contact</p>
        </div>
      </section>
      <section className="pt-16">
        <div className="container mx-auto">
          <div className="grid items-center grid-cols-12 mt-5 lg:gap-8 gap-y-8">
            <div className="col-span-12 lg:col-span-6">
              <div className="mt-4">
                <h3 className="mb-2 text-3xl text-gray-900 dark:text-white font-bold">
                  Get in touch
                </h3>
                <p className="text-gray-500 dark:text-gray-300">
                  Start working with Jobcy that can provide everything you need
                  to generate awareness, drive traffic, connect.
                </p>
                <form
                  method="post"
                  className="mt-4 contact-form"
                  name="myForm"
                  id="myForm"
                >
                  <span id="error-msg"></span>
                  <div className="grid grid-cols-12 gap-5">
                    <div className="col-span-12">
                      <div className="mb-3">
                        <label
                          htmlFor="nameInput"
                          className="text-gray-900 dark:text-gray-50"
                        >
                          Name
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className="w-full mt-1 rounded border border-gray-200 placeholder:text-sm placeholder:text-gray-400 dark:bg-transparent dark:border-gray-800 focus:outline-violet-500 focus:outline-1 dark:text-gray-200 px-2 py-2"
                          placeholder="Enter your name"
                        />
                      </div>
                    </div>
                    <div className="col-span-12 lg:col-span-6">
                      <div className="mb-3">
                        <label
                          htmlFor="emailInput"
                          className="text-gray-900 dark:text-gray-50"
                        >
                          Email
                        </label>
                        <input
                          type="email"
                          className="w-full mt-1 rounded border border-gray-200 placeholder:text-sm placeholder:text-gray-400 dark:bg-transparent dark:border-gray-800 focus:outline-violet-500 focus:outline-1 dark:text-gray-200 px-2 py-2"
                          id="emaiol"
                          name="email"
                          placeholder="Enter your email"
                        />
                      </div>
                    </div>
                    <div className="col-span-12 lg:col-span-6">
                      <div className="mb-3">
                        <label
                          htmlFor="subjectInput"
                          className="text-gray-900 dark:text-gray-50"
                        >
                          Subject
                        </label>
                        <input
                          type="text"
                          className="w-full mt-1 rounded border-gray-200 px-2 py-2 border placeholder:text-sm placeholder:text-gray-400 dark:bg-transparent dark:border-gray-800 focus:outline-violet-500 focus:outline-1 dark:text-gray-200"
                          id="subjectInput"
                          name="subject"
                          placeholder="Enter your subject"
                        />
                      </div>
                    </div>
                    <div className="col-span-12">
                      <div className="mb-3">
                        <label
                          htmlFor="meassageInput"
                          className="text-gray-900 dark:text-gray-50"
                        >
                          Your Message
                        </label>
                        <textarea
                          className="w-full mt-1 rounded border-gray-200 px-2 py-2 border placeholder:text-sm placeholder:text-gray-400 dark:bg-transparent dark:border-gray-800 focus:outline-violet-500 focus:outline-1 dark:text-gray-200"
                          id="meassageInput"
                          placeholder="Enter your message"
                          name="comments"
                          rows={3}
                        ></textarea>
                      </div>
                    </div>
                  </div>
                  <div className="text-right">
                    <button
                      type="submit"
                      id="submit"
                      name="submit"
                      className="text-white border-transparent btn group-data-[theme-color=violet]:bg-violet-500 group-data-[theme-color=sky]:bg-sky-500 group-data-[theme-color=red]:bg-red-500 group-data-[theme-color=green]:bg-green-500 group-data-[theme-color=pink]:bg-pink-500 group-data-[theme-color=blue]:bg-blue-500"
                    >
                      Send Message <i className="uil uil-message ms-1"></i>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-5 lg:ml-20 relative">
              <div className="text-center ">
                <Image
                  src={contactImg}
                  alt="contact"
                  width={500}
                  height={400}
                />
              </div>
              <div className="pt-3 mt-4">
                <div className="flex items-center mt-2 text-gray-500">
                  <div className="group-data-[theme-color=violet]:text-violet-500 group-data-[theme-color=sky]:text-sky-500 group-data-[theme-color=red]:text-red-500 group-data-[theme-color=green]:text-green-500 group-data-[theme-color=pink]:text-pink-500 group-data-[theme-color=blue]:text-blue-500 shrink-0 text-22">
                    <CSIcon className="mr-2 text-violet-500 text-[22px]" name="uil-map-marker" />
                  </div>
                  <div className="ltr:ml-2 rtl:mr-2 grow-1">
                    <p className="mb-0 dark:text-gray-300">
                      2453 Clinton StreetLittle Rock, California, USA
                    </p>
                  </div>
                </div>
                <div className="flex items-center mt-2 text-gray-500">
                  <div className="group-data-[theme-color=violet]:text-violet-500 group-data-[theme-color=sky]:text-sky-500 group-data-[theme-color=red]:text-red-500 group-data-[theme-color=green]:text-green-500 group-data-[theme-color=pink]:text-pink-500 group-data-[theme-color=blue]:text-blue-500 shrink-0 text-22">
                    <CSIcon className="mr-2 text-violet-500 text-[22px]" name="uil-envelope" />
                  </div>
                  <div className="ltr:ml-2 rtl:mr-2 grow-1">
                    <p className="mb-0 dark:text-gray-300">
                      contactus@Jobcy.com
                    </p>
                  </div>
                </div>
                <div className="flex items-center mt-2 text-gray-500">
                  <div className="group-data-[theme-color=violet]:text-violet-500 group-data-[theme-color=sky]:text-sky-500 group-data-[theme-color=red]:text-red-500 group-data-[theme-color=green]:text-green-500 group-data-[theme-color=pink]:text-pink-500 group-data-[theme-color=blue]:text-blue-500 shrink-0 text-22">
                    <CSIcon className="mr-2 text-violet-500 text-[22px]" name="uil-phone-alt" />
                  </div>
                  <div className="ltr:ml-2 rtl:mr-2 grow-1">
                    <p className="mb-0 dark:text-gray-300">(+245) 223 1245</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="pt-20">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.7090969330575!2d105.77899777589964!3d21.004294980638974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345352cf1903af%3A0xb4800e4a2b41a6eb!2zTmcuIDIgxJBMIFRoxINuZyBMb25nLCBN4buFIFRyw6wsIE5hbSBU4burIExpw6ptLCBIw6AgTuG7mWksIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1687713865485!5m2!1sen!2s"
            height="350"
            style={{
              border: "none",
              width: "100%",
            }}
            allowFullScreen={true}
            loading="lazy"
          ></iframe>
        </div>
      </section>
    </main>
  );
}
