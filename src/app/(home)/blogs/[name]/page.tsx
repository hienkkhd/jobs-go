import Image from "next/image";
import React from "react";
import bgHead from "@/assets/images/shape.png";
import imageBlog from "@/assets/images/img-15.jpg";
import CSCarousel from "@/app/components/Carousel";
import CSIcon from "@/app/components/Icon";
import Link from "next/link";

type Props = {};

function BlogDetail({}: Props) {
  return (
    <main>
      <section className="w-full bg-violet-500 pt-28 lg:pt-[300px] lg:py-[160px] relative overflow-hidden flex items-center justify-center flex-col">
        <Image
          src={bgHead}
          width={5000}
          height={40}
          alt="BG"
          className="absolute bottom-0"
        />
        <h1 className="text-white text-[26px] font-bold">Blog List</h1>
        <div className="flex gap-3 mt-2 items-center">
          <p className="text-white text-sm font-normal uppercase">Home</p>
          <p className="text-white text-sm font-normal uppercase">{">"}</p>
          <p className="text-white text-sm font-normal uppercase">Blog List</p>
        </div>
      </section>
      <section className="py-16">
        <div className="container mx-auto">
          <div className="grid grid-cols-12 md:gap-8">
            <div className="col-span-12 md:col-span-6 md:col-start-4">
              <div className="text-center">
                <p className="mb-0 font-semibold text-red-600">Fashion</p>
                <h3 className="font-bold text-gray-900 text-[26px] dark:text-white">
                  How to get creative in your work ?
                </h3>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-12 mt-8 md:gap-14">
            <div className="col-span-12 lg:col-span-8">
              <CSCarousel autoplay>
                <div className="relative w-full h-[500px]">
                  <Image fill src={imageBlog} alt="" className="rounded-lg" />
                </div>
                <div className="relative w-full h-[500px]">
                  <Image fill src={imageBlog} alt="" className="rounded-lg" />
                </div>
              </CSCarousel>
              <ul className="flex flex-wrap items-center mt-3 mb-0 text-gray-500">
                <li>
                  <div className="flex items-center gap-3">
                    <div className="shrink-0">
                      <Image
                        width={500}
                        height={500}
                        src={imageBlog}
                        alt=""
                        className="w-12 h-12 rounded-full"
                      />
                    </div>
                    <div>
                      <Link
                        href="blog-author.html"
                        className="text-gray-900 dark:text-white"
                      >
                        <h6 className="font-semibold mb-0 dark:text-gray-300">
                          By Alice Mellor
                        </h6>
                      </Link>
                    </div>
                  </div>
                </li>
                <li className="ml-3">
                  <div className="flex items-center">
                    <div className="shrink-0">
                      <CSIcon name="uil-calendar-alt" />
                    </div>
                    <div className="ml-2">
                      <p className="mb-0 dark:text-gray-300"> Aug 02, 2021</p>
                    </div>
                  </div>
                </li>
                <li className="ml-3">
                  <div className="flex items-center">
                    <div className="shrink-0">
                      <CSIcon name="uil-comments-alt" />
                    </div>
                    <div className="ml-2">
                      <p className="mb-0 dark:text-gray-300"> 2 Comments</p>
                    </div>
                  </div>
                </li>
              </ul>
              <div className="mt-4">
                <h5 className="mb-2 text-gray-900 dark:text-gray-50 font-semibold text-lg">
                  What makes the best co-working space?
                </h5>
                <p className="text-gray-500 dark:text-gray-300">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <p className="mb-4 text-gray-500 dark:text-gray-300">
                  Home renovations, especially those involving plentiful of
                  demolition can be a very dusty affair. The same is true as we
                  experience the emotional sensation of stress from our first
                  instances of social rejection ridicule. We quickly learn to
                  fear and thus automatically avoid potentially stressful
                  situations of all kinds, including the most common of all
                  making mistakes.
                </p>
                <figure className="text-center blog-blockquote">
                  <blockquote className="blockquote">
                    <p className="text-17 dark:text-gray-50 font-semibold">
                      &quot;A business consulting agency is involved in the
                      planning, implementation, and education of
                      businesses.&quot;
                    </p>
                  </blockquote>
                  <figcaption className="mb-4 text-base text-gray-500 blockquote-footer dark:text-gray-300">
                    — Creative Agency
                    <cite
                      title="Source Title"
                      className="font-semibold italic ml-2 text-violet-500"
                    >
                      Alice Mellor
                    </cite>
                  </figcaption>
                </figure>
                <p className="text-gray-500 dark:text-gray-300">
                  Whether article spirits new her covered hastily sitting her.
                  Money witty books nor son add. Chicken age had evening believe
                  but proceed pretend mrs. At missed advice my it no sister.
                  Miss told ham dull knew see she spot near can. Spirit her
                  entire her called.
                </p>
                <p className="text-gray-500 dark:text-gray-300">
                  The advantage of its Latin origin and the relative
                  meaninglessness of Lorum Ipsum is that the text does not
                  attract attention to itself or distract the viewer&qpos;s
                  attention from the layout.
                </p>
                <div className="flex items-center my-4">
                  <div className="shrink-0">
                    <b className="text-gray-900 dark:text-gray-50 mr-3">
                      Tags:
                    </b>
                  </div>
                  <div className="flex gap-2">
                    <Link
                      href="/"
                      className="px-1.5 py-0.5 mt-1 text-sm font-medium text-green-500 bg-green-500/20 rounded"
                    >
                      Business
                    </Link>
                    <Link
                      href="/"
                      className="px-1.5 py-0.5 mt-1 text-sm font-medium text-green-500 bg-green-500/20 rounded"
                    >
                      design
                    </Link>
                    <Link
                      href="/"
                      className="px-1.5 py-0.5 mt-1 text-sm font-medium text-green-500 bg-green-500/20 rounded"
                    >
                      Creative
                    </Link>
                    <Link
                      href="/"
                      className="px-1.5 py-0.5 mt-1 text-sm font-medium text-green-500 bg-green-500/20 rounded"
                    >
                      Event
                    </Link>
                  </div>
                </div>
                <ul className="flex gap-2 mb-0 md:justify-end items-center">
                  <li>
                    <b className="text-gray-900 dark:text-gray-50">
                      Share post:
                    </b>
                  </li>
                  <li className="p-2 bg-violet-500/20 rounded-md">
                    <CSIcon name="uil-facebook-f" className="text-violet-500" />
                  </li>
                  <li className="p-2 bg-violet-500/20 rounded-md">
                    <CSIcon name="uil-whatsapp" className="text-violet-500" />
                  </li>
                  <li className="p-2 bg-violet-500/20 rounded-md">
                    <CSIcon
                      name="uil-linkedin-alt"
                      className="text-violet-500"
                    />
                  </li>
                  <li className="p-2 bg-violet-500/20 rounded-md">
                    <CSIcon name="uil-envelope" className="text-violet-500" />
                  </li>
                  <li className="p-2 bg-violet-500/20 rounded-md">
                    <CSIcon
                      name="simple-icons:zalo"
                      className="text-violet-500"
                    />
                  </li>
                </ul>
                <h5 className="pb-3 mt-8 text-gray-900 border-b border-gray-100/50 dark:text-gray-50 dark:border-zinc-700">
                  Comments
                </h5>
                <div className="mt-8">
                  <div className="flex align-top">
                    <div className="shrink-0">
                      <Image
                        className="p-1 rounded-full w-14 h-14 ring-2 ring-gray-100/20"
                        width={500}
                        height={500}
                        src={imageBlog}
                        alt="img"
                      />
                    </div>
                    <div className="ml-3 grow">
                      <small className="text-xs text-gray-500 ltr:float-right rtl:float-left dark:text-gray-300">
                        <i className="uil uil-clock" /> 30 min Ago
                      </small>
                      <Link
                        href="/"
                        className="text-gray-900 transition-all duration-500 ease-in-out hover:bg-violet-500 dark:text-gray-50"
                      >
                        <h6 className="mb-0 text-16 mt-sm-0 font-semibold text-lg">
                          Rebecca Swartz
                        </h6>
                      </Link>
                      <p className="mb-0 text-sm text-gray-500 dark:text-gray-300">
                        Aug 10, 2021
                      </p>
                      <div className="my-3">
                        <Link
                          href="/"
                          className="bg-gray-50 px-1.5 py-1 text-xs rounded bg-violet-500/20 text-violet-500 font-semibold"
                        >
                          <i className="mdi mdi-reply text-violet-500" /> Reply
                        </Link>
                      </div>
                      <p className="mb-0 italic text-gray-500 dark:text-gray-300">
                        &quot; There are many variations of passages of Lorem
                        Ipsum available, but the majority have suffered
                        alteration in some form, by injected humour &quot;
                      </p>
                    </div>
                  </div>
                </div>
                <div className="mt-8">
                  <div className="flex align-top">
                    <div className="shrink-0">
                      <Image
                        className="p-1 rounded-full h-14 w-14 ring-2 ring-gray-100/20"
                        width={500}
                        height={500}
                        src={imageBlog}
                        alt="img"
                      />
                    </div>
                    <div className="ml-3 flex-grow-1">
                      <small className="text-xs text-gray-500 ltr:float-right rtl:float-left dark:text-gray-300">
                        <i className="uil uil-clock" /> 2 hrs Ago
                      </small>
                      <Link
                        href="/"
                        className="text-gray-900 transition-all duration-500 ease-in-out hover:bg-violet-500 dark:text-gray-50"
                      >
                        <h6 className="mb-0 text-16 font-semibold text-lg">
                          Adam Gibson
                        </h6>
                      </Link>
                      <p className="mb-0 text-sm text-gray-500 dark:text-gray-300">
                        Aug 10, 2021
                      </p>
                      <div className="my-3">
                        <Link
                          href="/"
                          className="bg-gray-50 px-1.5 py-1 text-xs rounded bg-violet-500/20 text-violet-500 font-semibold"
                        >
                          <i className="mdi mdi-reply text-violet-500" /> Reply
                        </Link>
                      </div>
                      <p className="mb-0 text-gray-500 fst-italic dark:text-gray-300">
                        &quot; The most important aspect of beauty was,
                        therefore, an inherent part of an object, rather than
                        something applied superficially, and was based on
                        universal, recognisable truths. &quot;
                      </p>
                      <div className="flex mt-8 align-top">
                        <div className="shrink-0">
                          <Image
                            className="p-1 rounded-full h-14 w-14 ring-2 ring-gray-100/20"
                            width={500}
                            height={500}
                            src={imageBlog}
                            alt="img"
                          />
                        </div>
                        <div className="ml-3 flex-grow-1">
                          <small className="text-xs text-gray-500 ltr:float-right rtl:float-left dark:text-gray-300">
                            <i className="uil uil-clock" /> 2 hrs Ago
                          </small>
                          <Link
                            href="/"
                            className="text-gray-900 transition-all duration-500 ease-in-out hover:bg-violet-500 dark:text-gray-50"
                          >
                            <h6 className="mb-0 text-16 font-semibold text-lg">
                              Kiera Finch
                            </h6>
                          </Link>
                          <p className="mb-0 text-sm text-gray-500 dark:text-gray-300">
                            Aug 10, 2021
                          </p>
                          <div className="my-3">
                            <Link
                              href="/"
                              className="bg-gray-50 px-1.5 py-1 text-xs rounded bg-violet-500/20 text-violet-500 font-semibold"
                            >
                              <i className="mdi mdi-reply text-violet-500" />{" "}
                              Reply
                            </Link>
                          </div>
                          <p className="mb-0 text-gray-500 fst-italic dark:text-gray-300">
                            &quot; This response is important for our ability to
                            learn from mistakes, but it alsogives rise to
                            self-criticism, because it is part of the
                            threat-protection system. &quot;
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <form action="#" className="mt-8 contact-form">
                    <h5 className="pb-3 text-gray-900 border-b border-gray-100/50 dark:text-gray-50 dark:border-zinc-700 font-bold">
                      Leave a Message
                    </h5>
                    <div className="grid grid-cols-12 gap-5 mt-6">
                      <div className="col-span-6">
                        <div className="relative mb-3">
                          <label
                            htmlFor="name"
                            className="text-gray-900 dark:text-gray-50"
                          >
                            Name
                          </label>
                          <input
                            name="name"
                            id="name"
                            type="text"
                            className="focus:outline-1 p-2 focus:outline-violet-500 w-full mt-1 rounded border-gray-500/50 placeholder:text-xs border"
                            placeholder="Enter your name*"
                            required={true}
                          />
                        </div>
                      </div>
                      <div className="col-span-6">
                        <div className="relative mb-3">
                          <label
                            htmlFor="email"
                            className="text-gray-900 dark:text-gray-50"
                          >
                            Email address
                          </label>
                          <input
                            name="email"
                            id="email"
                            type="email"
                            className="focus:outline-1 p-2 focus:outline-violet-500 w-full mt-1 rounded border-gray-500/50 placeholder:text-xs border"
                            placeholder="Enter your email*"
                            required={true}
                          />
                        </div>
                      </div>
                      <div className="col-span-12">
                        <div className="relative mb-3">
                          <label
                            htmlFor="subject"
                            className="text-gray-900 dark:text-gray-50"
                          >
                            Subject
                          </label>
                          <input
                            name="subject"
                            id="subject"
                            type="text"
                            className="focus:outline-1 p-2 focus:outline-violet-500 w-full mt-1 rounded border-gray-500/50 placeholder:text-xs border"
                            placeholder="Subject"
                          />
                        </div>
                      </div>
                      <div className="col-span-12">
                        <div className="relative mb-3">
                          <label
                            htmlFor="comments"
                            className="text-gray-900 dark:text-gray-50"
                          >
                            Meassage
                          </label>
                          <textarea
                            name="comments"
                            id="comments"
                            rows={4}
                            className="focus:outline-1 p-2 focus:outline-violet-500 w-full mt-1 rounded border-gray-500/50 placeholder:text-xs border"
                            placeholder="Enter your message"
                            defaultValue={""}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="flex justify-end mt-3">
                      <button
                        name="submit"
                        type="submit"
                        id="submit"
                        className="text-white bg-violet-500 rounded-md px-4 py-2 flex items-center gap-2 transition-all duration-500 ease-in-out hover:-translate-y-2"
                      >
                        Send Meassage <CSIcon name="uil-message" />
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-4">
              <div>
                <h6 className="mb-2 text-gray-900 text-16 dark:text-gray-50 font-bold text-[18px]">
                  Popular Post
                </h6>
                <div className="h-[2px] w-full bg-gray-500/20 relative after:contents[''] after:w-[20%] after:h-full after:bg-violet-500 after:absolute after:top-0 after:left-0"></div>
                <ul className="my-4">
                  <li className="flex flex-wrap items-center pb-4 mb-4 border-b gap-y-3 gap-x-2 md:gap-y-0 border-gray-100/50 dark:border-zinc-600">
                    <Image
                      width={500}
                      height={500}
                      src={imageBlog}
                      alt=""
                      className="w-[86px] object-cover h-16 rounded"
                    />
                    <div className="ltr:ml-3 rtl:mr-3 grow">
                      <Link
                        href="blog-details.html"
                        className="overflow-hidden font-medium text-gray-900 truncate dark:text-gray-50"
                      >
                        The evolution of landing page creativity
                      </Link>
                      <span className="block text-sm text-gray-500 dark:text-gray-300">
                        Aug 10, 2021
                      </span>
                    </div>
                  </li>
                  <li className="flex flex-wrap items-center pb-4 mb-4 border-b gap-y-3 gap-x-2 md:gap-y-0 border-gray-100/50 dark:border-zinc-600">
                    <Image
                      width={500}
                      height={500}
                      src={imageBlog}
                      alt=""
                      className="w-[86px] object-cover h-16 rounded"
                    />
                    <div className="ltr:ml-3 rtl:mr-3 grow">
                      <Link
                        href="blog-details.html"
                        className="overflow-hidden font-medium text-gray-900 truncate dark:text-gray-50"
                      >
                        Beautiful day with friends in paris
                      </Link>
                      <span className="block text-sm text-gray-500 dark:text-gray-300">
                        Jun 24, 2021
                      </span>
                    </div>
                  </li>
                  <li className="flex flex-wrap items-center pb-4 mb-4 border-b gap-y-3 gap-x-2 md:gap-y-0 border-gray-100/50 dark:border-zinc-600">
                    <Image
                      width={500}
                      height={500}
                      src={imageBlog}
                      alt=""
                      className="w-[86px] object-cover h-16 rounded"
                    />
                    <div className="ltr:ml-3 rtl:mr-3 grow">
                      <Link
                        href="blog-details.html"
                        className="overflow-hidden font-medium text-gray-900 truncate dark:text-gray-50"
                      >
                        Project discussion with team
                      </Link>
                      <span className="block text-sm text-gray-500 dark:text-gray-300">
                        July 13, 2021
                      </span>
                    </div>
                  </li>
                  <li className="flex flex-wrap items-center pb-4 mb-4 gap-y-3 gap-x-2 md:gap-y-0">
                    <Image
                      width={500}
                      height={500}
                      src={imageBlog}
                      alt=""
                      className="w-[86px] object-cover h-16 rounded"
                    />
                    <div className="ltr:ml-3 rtl:mr-3 grow">
                      <Link
                        href="blog-details.html"
                        className="overflow-hidden font-medium text-gray-900 truncate dark:text-gray-50"
                      >
                        Smartest Applications for Business
                      </Link>
                      <span className="block text-sm text-gray-500 dark:text-gray-300">
                        Feb 01, 2021
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="mt-8">
                <h6 className="mb-2 text-gray-900 text-16 dark:text-gray-50 font-bold text-[18px]">
                  Text Widget
                </h6>
                <div className="h-[2px] w-full bg-gray-500/20 relative after:contents[''] after:w-[20%] after:h-full after:bg-violet-500 after:absolute after:top-0 after:left-0"></div>
                <div className="my-4">
                  <p className="mt-3 mb-0 text-gray-500 dark:text-gray-300">
                    Exercitation photo booth stumptown tote bag Banksy, elit
                    small batch freegan sed. Craft beer elit seitan
                    exercitation, photo booth et 8-bit kale chips proident
                    chillwave deep v laborum. Aliquip veniam delectus, Marfa
                    eiusmod Pinterest in do umami readymade swag.
                  </p>
                </div>
              </div>
              <div className="mt-8">
                <h6 className="mb-2 text-gray-900 text-16 dark:text-gray-50 font-bold text-[18px]">
                  Archives
                </h6>
                <div className="h-[2px] w-full bg-gray-500/20 relative after:contents[''] after:w-[20%] after:h-full after:bg-violet-500 after:absolute after:top-0 after:left-0"></div>
                <ul className="mt-3 mb-0 text-gray-900 dark:text-gray-50">
                  <li className="py-1">
                    <Link
                      className="mr-2 text-gray-500 dark:text-gray-300 flex items-center"
                      href="/"
                    >
                      <CSIcon name="uil-angle-right-b" />
                      March 2021 (40)
                    </Link>
                  </li>
                  <li className="py-1">
                    <Link
                      className="mr-2 text-gray-500 dark:text-gray-300 flex items-center"
                      href="/"
                    >
                      <CSIcon name="uil-angle-right-b" />
                      April 2021 (40)
                    </Link>
                  </li>
                  <li className="py-1">
                    <Link
                      className="mr-2 text-gray-500 dark:text-gray-300 flex items-center"
                      href="/"
                    >
                      <CSIcon name="uil-angle-right-b" />
                      Nov 2020 (40)
                    </Link>
                  </li>
                  <li className="py-1">
                    <Link
                      className="mr-2 text-gray-500 dark:text-gray-300 flex items-center"
                      href="/"
                    >
                      <CSIcon name="uil-angle-right-b" />
                      May 2020 (40)
                    </Link>
                  </li>
                  <li className="py-1">
                    <Link
                      className="mr-2 text-gray-500 dark:text-gray-300 flex items-center"
                      href="/"
                    >
                      <CSIcon name="uil-angle-right-b" />
                      Jun 2019 (40)
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="mt-8">
                <h6 className="mb-2 text-gray-900 text-16 dark:text-gray-50 font-bold text-[18px]">
                  Latest Tags
                </h6>
                <div className="h-[2px] w-full bg-gray-500/20 relative after:contents[''] after:w-[20%] after:h-full after:bg-violet-500 after:absolute after:top-0 after:left-0"></div>
                <div className="flex flex-wrap gap-2 mt-3">
                  <Link
                    className="mt-2 text-xs font-medium px-2 py-0.5 text-center bg-gray-50 text-gray-600 rounded hover:bg-violet-500 hover:text-white transition-all duration-200 ease-in dark:bg-gray-500/20 dark:text-gray-100 dark:hover:bg-violet-500/20 dark:hover:text-white"
                    href="/"
                  >
                    Fashion
                  </Link>
                  <Link
                    className="mt-2 text-xs font-medium px-2 py-0.5 text-center bg-gray-50 text-gray-600 rounded hover:bg-violet-500 hover:text-white transition-all duration-200 ease-in dark:bg-gray-500/20 dark:text-gray-100 dark:hover:bg-violet-500/20 dark:hover:text-white"
                    href="/"
                  >
                    Jobs
                  </Link>
                  <Link
                    className="mt-2 text-xs font-medium px-2 py-0.5 text-center bg-gray-50 text-gray-600 rounded hover:bg-violet-500 hover:text-white transition-all duration-200 ease-in dark:bg-gray-500/20 dark:text-gray-100 dark:hover:bg-violet-500/20 dark:hover:text-white"
                    href="/"
                  >
                    Business
                  </Link>
                  <Link
                    className="mt-2 text-xs font-medium px-2 py-0.5 text-center bg-gray-50 text-gray-600 rounded hover:bg-violet-500 hover:text-white transition-all duration-200 ease-in dark:bg-gray-500/20 dark:text-gray-100 dark:hover:bg-violet-500/20 dark:hover:text-white"
                    href="/"
                  >
                    Corporate
                  </Link>
                  <Link
                    className="mt-2 text-xs font-medium px-2 py-0.5 text-center bg-gray-50 text-gray-600 rounded hover:bg-violet-500 hover:text-white transition-all duration-200 ease-in dark:bg-gray-500/20 dark:text-gray-100 dark:hover:bg-violet-500/20 dark:hover:text-white"
                    href="/"
                  >
                    E-commerce
                  </Link>
                  <Link
                    className="mt-2 text-xs font-medium px-2 py-0.5 text-center bg-gray-50 text-gray-600 rounded hover:bg-violet-500 hover:text-white transition-all duration-200 ease-in dark:bg-gray-500/20 dark:text-gray-100 dark:hover:bg-violet-500/20 dark:hover:text-white"
                    href="/"
                  >
                    Agency
                  </Link>
                  <Link
                    className="mt-2 text-xs font-medium px-2 py-0.5 text-center bg-gray-50 text-gray-600 rounded hover:bg-violet-500 hover:text-white transition-all duration-200 ease-in dark:bg-gray-500/20 dark:text-gray-100 dark:hover:bg-violet-500/20 dark:hover:text-white"
                    href="/"
                  >
                    Responsive
                  </Link>
                </div>
              </div>
              <div className="mt-8">
                <h6 className="mb-2 text-gray-900 text-16 dark:text-gray-50 font-bold text-[18px]">
                  Follow &amp; Connect
                </h6>
                <div className="h-[2px] w-full bg-gray-500/20 relative after:contents[''] after:w-[20%] after:h-full after:bg-violet-500 after:absolute after:top-0 after:left-0"></div>
                <ul className="flex flex-wrap gap-3 mt-4">
                  <Link
                    href="/"
                    className="leading-9 text-center ease-in rounded-full w-12 h-12 bg-violet-50 flex items-center justify-center transition-all hover:bg-violet-500 hover:text-white"
                  >
                    <CSIcon className="text-xl" name="uil-facebook-f" />
                  </Link>

                  <Link
                    href="/"
                    className="leading-9 text-center ease-in rounded-full w-12 h-12 bg-violet-50 flex items-center justify-center transition-all hover:bg-violet-500 hover:text-white"
                  >
                    <CSIcon className="text-xl" name="uil-whatsapp" />
                  </Link>
                  <Link
                    href="/"
                    className="leading-9 text-center ease-in rounded-full w-12 h-12 bg-violet-50 flex items-center justify-center transition-all hover:bg-violet-500 hover:text-white"
                  >
                    <CSIcon className="text-xl" name="uil-twitter-alt" />
                  </Link>
                  <Link
                    href="/"
                    className="leading-9 text-center ease-in rounded-full w-12 h-12 bg-violet-50 flex items-center justify-center transition-all hover:bg-violet-500 hover:text-white"
                  >
                    <CSIcon className="text-xl" name="uil-dribbble" />
                  </Link>
                  <Link
                    href="/"
                    className="leading-9 text-center ease-in rounded-full w-12 h-12 bg-violet-50 flex items-center justify-center transition-all hover:bg-violet-500 hover:text-white"
                  >
                    <CSIcon className="text-xl" name="uil-envelope" />
                  </Link>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}

export default BlogDetail;
