import CSPagination from "@/app/components/Pagination";
import avartartImg from "@/assets/images/avatar.jpg";
import imgBlog from "@/assets/images/img-04.jpg";
import bgHead from "@/assets/images/shape.png";
import Image from "next/image";

type Props = {};

export default async function Page({}: Props) {

  return (
    <main>
      <section className="w-full bg-violet-500 pt-28 lg:pt-[300px] lg:py-[160px] relative overflow-hidden flex items-center justify-center flex-col">
        <Image
          src={bgHead}
          width={3500}
          height={40}
          alt="BG"
          className="absolute bottom-0"
        />
        <h1 className="text-white text-[26px] font-bold">Blog List</h1>
        <div className="flex gap-3 mt-2 items-center">
          <p className="text-white text-sm font-normal uppercase">Home</p>
          <p className="text-white text-sm font-normal uppercase">{">"}</p>
          <p className="text-white text-sm font-normal uppercase">Blog List</p>
        </div>
      </section>
      <section className="mt-12">
        <div className="container mx-auto">
          <div className="grid items-center grid-cols-12 gap-y-4 lg:gap-4">
            <div className="col-span-12">
              <div className="mb-2">
                <h4 className="text-[22.5px] text-gray-900 font-semibold dark:text-gray-50">
                  Latest &amp; Trending Blog Post
                </h4>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-7">
              <div className="relative mb-3 overflow-hidden rounde w-full pr-4 ">
                <Image src={imgBlog} alt="Blog" width={1000} height={400} />
              </div>
            </div>
            <div className="col-span-12 lg:col-span-5">
              <div>
                <span className="text-gray-500 dark:text-gray-300 font-bold">
                  Product
                </span>
                <span> - Aug 01, 2021</span>
              </div>
              <h2 className="text-lg font-bold mb-2">
                Do traditional landing pages work for Saas startups?
              </h2>
              <p className="text-gray-500 dark:text-gray-300 text-[16px]">
                Objectively pursue diverse catalysts for change for
                interoperable meta-services. Distinctively re-engineer
                revolutionary meta-services and premium architectures.
                Intrinsically incubate intuitive opportunities and real-time
                potentialities. Appropriately communicate one-to-one technology.
              </p>
              <div className="flex items-center gap-2 mt-5">
                <Image
                  src={avartartImg}
                  alt="Avatar"
                  width={56}
                  height={56}
                  className="rounded-full"
                />
                <div>
                  <h3 className="text-[16px] font-bold">James Lemire </h3>
                  <p className="mb-0 text-[14px]">Product Manager</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="mt-12 pb-12">
        <div className="container mx-auto">
          <div className="col-span-12 mb-5">
            <h4 className="text-[22.5px] text-gray-900 font-semibold dark:text-gray-50">
              All Blog Post
            </h4>
          </div>
          <div className="grid grid-cols-12 gap-y-10 lg:gap-x-5 lg:gap-y-10">
            <div className="col-span-12 lg:col-span-6">
              <div className="col-span-12 lg:col-span-7">
                <div className="relative mb-3 overflow-hidden rounde w-full pr-4 ">
                  <Image src={imgBlog} alt="Blog" width={1000} height={400} />
                </div>
              </div>
              <div className="col-span-12 lg:col-span-5">
                <div>
                  <span className="text-gray-500 dark:text-gray-300 font-bold">
                    Product
                  </span>
                  <span> - Aug 01, 2021</span>
                </div>
                <h2 className="text-lg font-bold mb-2">
                  Do traditional landing pages work for Saas startups?
                </h2>
                <p className="text-gray-500 dark:text-gray-300 text-[16px]">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <div className="flex items-center gap-2 mt-5">
                  <Image
                    src={avartartImg}
                    alt="Avatar"
                    width={56}
                    height={56}
                    className="rounded-full"
                  />
                  <div>
                    <h3 className="text-[16px] font-bold">James Lemire </h3>
                    <p className="mb-0 text-[14px]">Product Manager</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-6">
              <div className="col-span-12 lg:col-span-7">
                <div className="relative mb-3 overflow-hidden rounde w-full pr-4 ">
                  <Image src={imgBlog} alt="Blog" width={1000} height={400} />
                </div>
              </div>
              <div className="col-span-12 lg:col-span-5">
                <div>
                  <span className="text-gray-500 dark:text-gray-300 font-bold">
                    Product
                  </span>
                  <span> - Aug 01, 2021</span>
                </div>
                <h2 className="text-lg font-bold mb-2">
                  Do traditional landing pages work for Saas startups?
                </h2>
                <p className="text-gray-500 dark:text-gray-300 text-[16px]">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <div className="flex items-center gap-2 mt-5">
                  <Image
                    src={avartartImg}
                    alt="Avatar"
                    width={56}
                    height={56}
                    className="rounded-full"
                  />
                  <div>
                    <h3 className="text-[16px] font-bold">James Lemire </h3>
                    <p className="mb-0 text-[14px]">Product Manager</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-6">
              <div className="col-span-12 lg:col-span-7">
                <div className="relative mb-3 overflow-hidden rounde w-full pr-4 ">
                  <Image src={imgBlog} alt="Blog" width={1000} height={400} />
                </div>
              </div>
              <div className="col-span-12 lg:col-span-5">
                <div>
                  <span className="text-gray-500 dark:text-gray-300 font-bold">
                    Product
                  </span>
                  <span> - Aug 01, 2021</span>
                </div>
                <h2 className="text-lg font-bold mb-2">
                  Do traditional landing pages work for Saas startups?
                </h2>
                <p className="text-gray-500 dark:text-gray-300 text-[16px]">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <div className="flex items-center gap-2 mt-5">
                  <Image
                    src={avartartImg}
                    alt="Avatar"
                    width={56}
                    height={56}
                    className="rounded-full"
                  />
                  <div>
                    <h3 className="text-[16px] font-bold">James Lemire </h3>
                    <p className="mb-0 text-[14px]">Product Manager</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-6">
              <div className="col-span-12 lg:col-span-7">
                <div className="relative mb-3 overflow-hidden rounde w-full pr-4 ">
                  <Image src={imgBlog} alt="Blog" width={1000} height={400} />
                </div>
              </div>
              <div className="col-span-12 lg:col-span-5">
                <div>
                  <span className="text-gray-500 dark:text-gray-300 font-bold">
                    Product
                  </span>
                  <span> - Aug 01, 2021</span>
                </div>
                <h2 className="text-lg font-bold mb-2">
                  Do traditional landing pages work for Saas startups?
                </h2>
                <p className="text-gray-500 dark:text-gray-300 text-[16px]">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <div className="flex items-center gap-2 mt-5">
                  <Image
                    src={avartartImg}
                    alt="Avatar"
                    width={56}
                    height={56}
                    className="rounded-full"
                  />
                  <div>
                    <h3 className="text-[16px] font-bold">James Lemire </h3>
                    <p className="mb-0 text-[14px]">Product Manager</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-6">
              <div className="col-span-12 lg:col-span-7">
                <div className="relative mb-3 overflow-hidden rounde w-full pr-4 ">
                  <Image src={imgBlog} alt="Blog" width={1000} height={400} />
                </div>
              </div>
              <div className="col-span-12 lg:col-span-5">
                <div>
                  <span className="text-gray-500 dark:text-gray-300 font-bold">
                    Product
                  </span>
                  <span> - Aug 01, 2021</span>
                </div>
                <h2 className="text-lg font-bold mb-2">
                  Do traditional landing pages work for Saas startups?
                </h2>
                <p className="text-gray-500 dark:text-gray-300 text-[16px]">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <div className="flex items-center gap-2 mt-5">
                  <Image
                    src={avartartImg}
                    alt="Avatar"
                    width={56}
                    height={56}
                    className="rounded-full"
                  />
                  <div>
                    <h3 className="text-[16px] font-bold">James Lemire </h3>
                    <p className="mb-0 text-[14px]">Product Manager</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-12 lg:col-span-6">
              <div className="col-span-12 lg:col-span-7">
                <div className="relative mb-3 overflow-hidden rounde w-full pr-4 ">
                  <Image src={imgBlog} alt="Blog" width={1000} height={400} />
                </div>
              </div>
              <div className="col-span-12 lg:col-span-5">
                <div>
                  <span className="text-gray-500 dark:text-gray-300 font-bold">
                    Product
                  </span>
                  <span> - Aug 01, 2021</span>
                </div>
                <h2 className="text-lg font-bold mb-2">
                  Do traditional landing pages work for Saas startups?
                </h2>
                <p className="text-gray-500 dark:text-gray-300 text-[16px]">
                  Objectively pursue diverse catalysts for change for
                  interoperable meta-services. Distinctively re-engineer
                  revolutionary meta-services and premium architectures.
                  Intrinsically incubate intuitive opportunities and real-time
                  potentialities. Appropriately communicate one-to-one
                  technology.
                </p>
                <div className="flex items-center gap-2 mt-5">
                  <Image
                    src={avartartImg}
                    alt="Avatar"
                    width={56}
                    height={56}
                    className="rounded-full"
                  />
                  <div>
                    <h3 className="text-[16px] font-bold">James Lemire </h3>
                    <p className="mb-0 text-[14px]">Product Manager</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mt-10 flex justify-center items-center">
            <CSPagination total={10} />
          </div>
        </div>
      </section>
    </main>
  );
}
