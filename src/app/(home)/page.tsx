import Image from "next/image";
import Link from "next/link";
import { Suspense } from "react";
import banner from "../../assets/images/bg-shape.png";
import process01 from "../../assets/images/process-01.png";
import process02 from "../../assets/images/process-02.png";
import logoBrand from "../../assets/images/wordpress.svg";
import CSCarousel from "../components/Carousel";
import CSIcon from "../components/Icon";
import CSSelect from "../components/Select";
import Categories from "./components/Categories";
import RandomJobs from "./components/RandomJobs";
import Loading from "./components/RandomJobs/Loading";
import HowWork from "./components/HowWork";
export default function Home() {
  return (
    <main>
      <section className="w-full bg-violet-500/20 my-10 lg:py-[160px] relative overflow-hidden">
        <div className="container px-5 lg:px-24 mx-auto grid items-center grid-cols-12">
          <div className="col-span-12 lg:col-span-7">
            <h3 className="mb-3 uppercase text-sm text-gray-900 font-semibold">
              WE HAVE 150,000+ LIVE JOBS
            </h3>
            <h1 className="mb-3 text-5xl font-semibold leading-tight text-gray-700 dark:text-gray-50 ">
              Find your dream jobs <br />
              with
              <span className="text-sky-500 font-bold"> Jobcy</span>
            </h1>
            <h2 className="text-lg font-light text-gray-500">
              Find jobs, create trackable resumes and enrich your <br />
              applications.Carefully crafted after analyzing the needs of
              different
              <br /> industries.
            </h2>
            <form className="mt-3 rounded-md grid grid-cols-12 items-center overflow-hidden gap-y-2">
              <div className="col-span-12 md:col-span-6 xl:col-span-4 flex items-center px-3 relative h-[60px] bg-white">
                <CSIcon
                  name="uil:bag"
                  className="text-blue-500"
                  fontSize={22}
                />
                <input
                  type="search"
                  name="search"
                  className="absolute border-none h-full right-0 border-transparent outline-none w-[calc(100%-42px)] placeholder:text-sm"
                  placeholder="Job, Company name..."
                />
              </div>
              <div className="col-span-12 md:col-span-6 xl:col-span-4 flex items-center px-3 relative h-[60px] bg-white">
                <CSIcon
                  name="uil:location-point"
                  className="text-blue-500"
                  fontSize={22}
                />
                <CSSelect />
              </div>
              <button className="col-span-12 xl:col-span-4 flex items-center justify-center bg-blue-500 h-[60px]">
                <CSIcon name="uil:search" color="white" fontSize={18} />
                <p className="ml-2 text-white text-lg">Find Job</p>
              </button>
            </form>
          </div>
          <div className="col-span-12 lg:col-span-5 relative h-[600px]">
            <Image
              src={process02}
              width={650}
              height={560}
              alt="Process"
              style={{
                maxWidth: "none",
              }}
            />
          </div>
        </div>
        <Image
          style={{
            position: "absolute",
            right: 0,
            bottom: "-40px",
            width: "100%",
          }}
          src={banner}
          alt="Process"
        />
      </section>
      <section>
        <div className="container px-5 py-20 lg:px-24 mx-auto">
          <div>
            <h3 className="text-center text-3xl font-semibold text-gray-900 dark:text-gray-50 mb-3">
              Browser Jobs Categories
            </h3>
            <p className="text-center mb-5 text-gray-500 whitespace-pre-line text-md">
              Post a job to tell us about your project. We{"'"}ll quickly match
              you with
              <br /> the right freelancers.
            </p>
          </div>
          <Categories />

          <div className="flex justify-center">
            <button className="mt-5 px-4 py-3 bg-violet-500 flex items-center gap-3 rounded-md text-white">
              Browse All Categories{" "}
              <CSIcon name="formkit:arrowright" fontSize={12} />
            </button>
          </div>
        </div>
      </section>
      <section className="py-20 bg-gray-50 dark:bg-neutral-700">
        <div className="container px-5 lg:px-24 mx-auto">
          <div>
            <h3 className="text-center text-3xl font-semibold text-gray-900 dark:text-gray-50 mb-3">
              New & Random Jobs
            </h3>
            <p className="text-center mb-5 text-gray-500 whitespace-pre-line text-md">
              Post a job to tell us about your project. We{"'"}ll quickly match
              you with
              <br /> the right freelancers.
            </p>
          </div>
          <Suspense fallback={<Loading />}>
            <RandomJobs />
          </Suspense>
          <div className="flex justify-center mt-4">
            <button className="mt-5 px-4 py-3 bg-violet-500 flex items-center gap-3 rounded-md text-white">
              View more
              <CSIcon name="formkit:arrowright" fontSize={12} />
            </button>
          </div>
        </div>
      </section>
      <section className="py-20 dark:bg-neutral-700">
        <div className="container px-5 py-20 lg:px-24 mx-auto grid grid-cols-12">
          <div className="col-span-12 lg:col-span-6">
            <h3 className="text-3xl font-semibold text-gray-900 dark:text-gray-50 mb-3">
              How It Work
            </h3>
            <p className="mb-5 text-gray-500 whitespace-pre-line text-md">
              Post a job to tell us about your project. We{"'"}ll quickly match
              you with
              <br /> the right freelancers.
            </p>
            <HowWork />
          </div>
          <div className="col-span-12 lg:col-span-6 relative">
            <Image src={process01} alt="" height={680} width={520} />
          </div>
        </div>
      </section>
      <section className="py-20 bg-gray-50 dark:bg-neutral-700">
        <h3 className="text-center text-3xl text-sky-500 font-semibold mb-5">
          Browse Our <span className="text-yellow-500 ">5000+</span> Latest Jobs
        </h3>
        <p className="text-md font-medium text-gray-500 text-center">
          Post a job to tell us about your project. We{"'"}ll quickly match you
          with the right freelancers.
        </p>
        <div className="flex justify-center mt-7">
          <Link
            href={"/"}
            className="bg-blue-500 text-white px-6 py-3 rounded-md flex items-center gap-2 transition duration-500 ease-in-out hover:-translate-y-2"
          >
            Start Now <CSIcon name="ion:rocket" />
          </Link>
        </div>
      </section>
      <section className="py-20">
        <div className="container px-5 lg:px-24 mx-auto grid grid-cols-12">
          <div className="col-span-12">
            <h3 className="text-center text-3xl font-semibold text-gray-900 dark:text-gray-50 mb-3">
              Happy Candidates
            </h3>
            <p className="text-center mb-5 text-gray-500 whitespace-pre-line text-md">
              Post a job to tell us about your project. We{"'"}ll quickly match
              you with
              <br /> the right freelancers.
            </p>
          </div>
          <div className="col-span-8 col-start-3">
            <CSCarousel>
              <div className="py-5 mb-10">
                <div className="flex justify-center mb-5">
                  <Image src={logoBrand} alt="logo" width={210} height={50} />
                </div>
                <p className="text-lg font-normal text-center text-gray-500 mb-2">
                  &quot; Very well thought out and articulate communication.
                  Clear milestones, deadlines and fast work. Patience. Infinite
                  patience. No shortcuts. Even if the client is being careless.
                  &quot;
                </p>
                <p className="text-lg font-medium text-gray-900 text-center">
                  Jeffrey Montgomery
                </p>
                <p className="text-[15px] font-medium text-gray-500 text-center">
                  Creative Designer
                </p>
              </div>
              <div className="py-5 mb-10">
                <div className="flex justify-center mb-5">
                  <Image src={logoBrand} alt="logo" width={210} height={50} />
                </div>
                <p className="text-lg font-normal text-center text-gray-500 mb-2">
                  &quot; Very well thought out and articulate communication.
                  Clear milestones, deadlines and fast work. Patience. Infinite
                  patience. No shortcuts. Even if the client is being careless.
                  &quot;
                </p>
                <p className="text-lg font-medium text-gray-900 text-center">
                  Jeffrey Montgomery
                </p>
                <p className="text-[15px] font-medium text-gray-500 text-center">
                  Creative Designer
                </p>
              </div>
              <div className="py-5 mb-10">
                <div className="flex justify-center mb-5">
                  <Image src={logoBrand} alt="logo" width={210} height={50} />
                </div>
                <p className="text-lg font-normal text-center text-gray-500 mb-2">
                  &quot; Very well thought out and articulate communication.
                  Clear milestones, deadlines and fast work. Patience. Infinite
                  patience. No shortcuts. Even if the client is being careless.
                  &quot;
                </p>
                <p className="text-lg font-medium text-gray-900 text-center">
                  Jeffrey Montgomery
                </p>
                <p className="text-[15px] font-medium text-gray-500 text-center">
                  Creative Designer
                </p>
              </div>
            </CSCarousel>
          </div>
        </div>
      </section>
      <section className="py-20 bg-gray-50 dark:bg-neutral-700">
        <div className="container px-5 lg:px-24 mx-auto">
          <div>
            <h3 className="text-center text-3xl font-semibold text-gray-900 dark:text-gray-50 mb-3">
              Quick Career Tips
            </h3>
            <p className="text-center mb-5 text-gray-500 whitespace-pre-line text-md">
              Post a job to tell us about your project. We{"'"}ll quickly match
              you with
              <br /> the right freelancers.
            </p>
          </div>
          <div className="grid grid-cols-12 gap-5 mt-14">
            <div className="col-span-4 p-2 pb-6 bg-white shadow-md rounded-sm">
              <Image
                alt="example"
                src="https://themesdesign.in/jobcy-tailwind/layout/assets/images/blog/img-01.jpg"
                width={500}
                height={600}
                className="rounded-md"
              />
              <div className="mt-4 px-4">
                <h3 className="text-lg font-semibold text-gray-800">
                  How apps is the IT world ?
                </h3>
                <p className="text-[15px] font-normal text-gray-500 mt-2 mb-2">
                  The final text is not yet avaibookmark-label. <br /> Dummy
                  texts have Internet tend been in use by typesetters.
                </p>
                <Link href={"/"} className="text-sky-500 text-sm font-semibold">
                  Read more {">"}
                </Link>
              </div>
            </div>
            <div className="col-span-4 p-2 pb-6 bg-white shadow-md rounded-sm">
              <Image
                alt="example"
                src="https://themesdesign.in/jobcy-tailwind/layout/assets/images/blog/img-01.jpg"
                width={500}
                height={600}
                className="rounded-md"
              />
              <div className="mt-4 px-4">
                <h3 className="text-lg font-semibold text-gray-800">
                  How apps is the IT world ?
                </h3>
                <p className="text-[15px] font-normal text-gray-500 mt-2 mb-2">
                  The final text is not yet avaibookmark-label. <br /> Dummy
                  texts have Internet tend been in use by typesetters.
                </p>
                <Link href={"/"} className="text-sky-500 text-sm font-semibold">
                  Read more {">"}
                </Link>
              </div>
            </div>
            <div className="col-span-4 p-2 pb-6 bg-white shadow-md rounded-sm">
              <Image
                alt="example"
                src="https://themesdesign.in/jobcy-tailwind/layout/assets/images/blog/img-01.jpg"
                width={500}
                height={600}
                className="rounded-md"
              />
              <div className="mt-4 px-4">
                <h3 className="text-lg font-semibold text-gray-800">
                  How apps is the IT world ?
                </h3>
                <p className="text-[15px] font-normal text-gray-500 mt-2 mb-2">
                  The final text is not yet avaibookmark-label. <br /> Dummy
                  texts have Internet tend been in use by typesetters.
                </p>
                <Link href={"/"} className="text-sky-500 text-sm font-semibold">
                  Read more {">"}
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="py-10">
        <div className="container px-5 lg:px-24 mx-auto grid grid-cols-12">
          <div className="col-span-2 flex justify-center">
            <Image src={logoBrand} width={140} height={50} alt="logo"></Image>
          </div>
          <div className="col-span-2 flex justify-center">
            <Image src={logoBrand} width={140} height={50} alt="logo"></Image>
          </div>
          <div className="col-span-2 flex justify-center">
            <Image src={logoBrand} width={140} height={50} alt="logo"></Image>
          </div>
          <div className="col-span-2 flex justify-center">
            <Image src={logoBrand} width={140} height={50} alt="logo"></Image>
          </div>
          <div className="col-span-2 flex justify-center">
            <Image src={logoBrand} width={140} height={50} alt="logo"></Image>
          </div>
          <div className="col-span-2 flex justify-center">
            <Image src={logoBrand} width={140} height={50} alt="logo"></Image>
          </div>
        </div>
      </section>
    </main>
  );
}
