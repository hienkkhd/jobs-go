"use client";
import CSIcon from "@/app/components/Icon";
import { useState, ReactElement } from "react";

type Props = {
  title: string;
  items: ReactElement | ReactElement[];
  contanerClassname?: string;
};

function FilterCollapse({ title, items, contanerClassname }: Props) {
  const [isOpen, setIsOpen] = useState<boolean>(true);
  return (
    <div className={`overflow-hidden ${contanerClassname}`}>
      <div
        className={`flex justify-between h-[40px] items-center px-4 bg-violet-500/20 `}
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <p>{title}</p>
        <CSIcon
          name="ri:arrow-down-s-line"
          className={`${!isOpen && "rotate-180"} transition-all duration-500`}
          fontSize={20}
        />
      </div>
      <div
        className={`overflow-hidden ${
          !isOpen ? "h-[0px]" : "h-[100%]"
        } transition-all duration-500`}
      >
        <div
          className={`${
            !isOpen && "translate -translate-y-[100%]"
          } transition-all ease-in-out duration-500 p-4`}
        >
          {items}
        </div>
      </div>
    </div>
  );
}

export default FilterCollapse;
