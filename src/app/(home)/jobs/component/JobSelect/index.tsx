"use client";
import { Select } from "antd";
import React from "react";
import "./style.css";
import CSIcon from "@/app/components/Icon";
type Props = {};

function JobSelect({}: Props) {
  return (
    <Select
      showSearch
      style={{
        width: "100%",
      }}
      suffixIcon={<CSIcon name="uil:bag" fontSize={18} className="text-violet-500"/>}
      size="large"
      placeholder={"Select location"}
      optionFilterProp="children"
      filterOption={(input, option) =>
        (option?.title.toLowerCase() ?? "").includes(input.toLowerCase())
      }
      options={[
        {
          value: "1",
          label: "Vietnam",
          title: "Vietnam",
        },
        {
          value: "2",
          label: "Korea",
          title: "korea",
        },
        {
          value: "3",
          label: "Japan",
          title: "japan",
        },
      ]}
    />
  );
}

export default JobSelect;
