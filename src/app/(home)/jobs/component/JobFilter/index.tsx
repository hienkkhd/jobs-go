import React from "react";
import FilterCollapse from "../FilterCollapse";
import { Checkbox } from "antd";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import queryString from "query-string";
import { JOB_TYPE } from "@prisma/client";
import Link from "next/link";
type Props = {};

function JobFilter({}: Props) {
  const router = useRouter();
  const pathName = usePathname();
  const searchParam = useSearchParams();
  const parsed = queryString.parse(searchParam.toString());

  const options = [
    { label: "Apple", value: "Apple" },
    { label: "Pear", value: "Pear" },
    { label: "Orange", value: "Orange" },
  ];
  const experienceOptions = [
    { value: "", label: "No experience" },
    { value: "0-3", label: "0-3 years" },
    { value: "3-6", label: "3-6 years" },
    { value: ">6", label: "More than 6 years" },
  ];
  const JobTyeOption = [
    { value: "", label: "All" },
    { value: JOB_TYPE.FULLTIME, label: "Fulltime" },
    { value: JOB_TYPE.PARTTIME, label: "Parttime" },
    { value: JOB_TYPE.FREELANCER, label: "Freelancer" },
  ];
  const datePostOption = [
    { value: "", label: "All" },
    { value: "last-hour", label: "Last Hour" },
    { value: "last-24-hour", label: "Last 24 hours" },
    { value: "last-7-days", label: "Last 7 days" },
    { value: "last-14-days", label: "Last 14 days" },
    { value: "last-30-days", label: "Last 30 days" },
  ];
  return (
    <div className="gap-4 col-span-12 xl:col-span-3 relative flex flex-col">
      <FilterCollapse
        title="Location"
        items={
          <Checkbox.Group
            name="location"
            rootClassName="flex-col gap-3"
            options={options}
            defaultValue={searchParam.getAll("location")}
            onChange={(value) => {
              const newSearch = { ...parsed, location: value };
              router.push(pathName + "?" + queryString.stringify(newSearch));
            }}
          />
        }
      />
      <FilterCollapse
        title=" Work experience"
        items={
          <Checkbox.Group
            name="experience"
            rootClassName="flex-col gap-3"
            options={experienceOptions}
            defaultValue={searchParam.getAll("experience")}
            onChange={(value) => {
              const newSearch = { ...parsed, experience: value };
              router.push(pathName + "?" + queryString.stringify(newSearch));
            }}
          />
        }
      />
      <FilterCollapse
        title="Type of employment"
        items={
          <Checkbox.Group
            name="jobType"
            rootClassName="flex-col gap-3"
            options={JobTyeOption}
            defaultValue={searchParam.getAll("jobType")}
            onChange={(value) => {
              const newSearch = { ...parsed, jobType: value };
              router.push(pathName + "?" + queryString.stringify(newSearch));
            }}
          />
        }
      />
      <FilterCollapse
        title="Posted Time"
        items={
          <Checkbox.Group
            name="postedTime"
            rootClassName="flex-col gap-3"
            options={datePostOption}
            defaultValue={searchParam.getAll("postedTime")}
            onChange={(value) => {
              const newSearch = { ...parsed, postedTime: value };
              router.push(pathName + "?" + queryString.stringify(newSearch));
            }}
          />
        }
      />
    </div>
  );
}

export default JobFilter;
