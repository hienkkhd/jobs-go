"use client";
import CSIcon from "@/app/components/Icon";
import JobFilter from "../JobFilter";
import JobSelect from "../JobSelect";
import useSWR from "swr";
import "./style.css";
import { useSearchParams } from "next/navigation";
import { fetcher } from "@/lib/fetcher";
import JobCard from "@/app/components/JobCard";
import { JobPost } from "@prisma/client";
import Loading from "@/app/(home)/components/RandomJobs/Loading";
type Props = {};

function JobList({}: Props) {
  const searchParams = useSearchParams();
  const { data, isLoading, error } = useSWR(
    `/api/jobs?location=${searchParams.get("location")}`,
    fetcher
  );
  const jobList = data?.data;

  return (
    <section className="py-16">
      <div className="container mx-auto overflow-hidden">
        <div className="grid grid-cols-12 gap-y-10 lg:gap-10 w-full">
          <div className="col-span-12 xl:col-span-9">
            <div className="grid grid-cols-12 gap-3">
              <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 flex items-center relative h-[40px] bg-white">
                <CSIcon
                  name="uil:bag"
                  className="text-blue-500 absolute right-0 mx-2"
                  fontSize={18}
                />
                <input
                  type="search"
                  name="search"
                  className="border-violet-400/20 focus:border-violet-500 rounded-md border-[1px] h-full outline-none w-[100%] pr-[36px] pl-4 placeholder:text-sm"
                  placeholder="Job, Company name..."
                />
              </div>
              <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 flex items-center relative h-[40px] bg-white">
                <JobSelect />
              </div>
              <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 flex items-center relative h-[40px] bg-white">
                <JobSelect />
              </div>
              <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 flex items-center relative h-[40px] bg-white text-white">
                <button className="w-full bg-violet-500 h-full rounded-md text-md font-medium flex items-center justify-center">
                  <CSIcon name="uil:bag" className=" mx-2" fontSize={22} />
                  Filter
                </button>
              </div>
            </div>
            <div className="mt-10">
              <p className="text-lg font-medium">Popular</p>
              <div className="flex flex-wrap mt-5 gap-3">
                <div className="flex items-center gap-2 p-[6px] rounded overflow-hidden border border-gray-200">
                  <p className="w-[32px] h-[32px] bg-blue-500/20 flex justify-center items-center rounded-md">
                    25
                  </p>
                  <p className="text-[15px] font-semibold">UI/UX Designer</p>
                </div>
                <div className="flex items-center gap-2 p-[6px] rounded overflow-hidden border border-gray-200">
                  <p className="w-[32px] h-[32px] bg-blue-500/20 flex justify-center items-center rounded-md">
                    25
                  </p>
                  <p className="text-[15px] font-semibold">UI/UX Designer</p>
                </div>
                <div className="flex items-center gap-2 p-[6px] rounded overflow-hidden border border-gray-200">
                  <p className="w-[32px] h-[32px] bg-blue-500/20 flex justify-center items-center rounded-md">
                    25
                  </p>
                  <p className="text-[15px] font-semibold">UI/UX Designer</p>
                </div>
                <div className="flex items-center gap-2 p-[6px] rounded overflow-hidden border border-gray-200">
                  <p className="w-[32px] h-[32px] bg-blue-500/20 flex justify-center items-center rounded-md">
                    25
                  </p>
                  <p className="text-[15px] font-semibold">UI/UX Designer</p>
                </div>
                <div className="flex items-center gap-2 p-[6px] rounded overflow-hidden border border-gray-200">
                  <p className="w-[32px] h-[32px] bg-blue-500/20 flex justify-center items-center rounded-md">
                    25
                  </p>
                  <p className="text-[15px] font-semibold">UI/UX Designer</p>
                </div>
              </div>
            </div>
            <div className="mt-10">
              {!isLoading ? (
                jobList?.map((item: JobPost, index: number) => {
                  return (
                    <JobCard
                      key={index}
                      title={item.title}
                      jobType={item.jobType}
                      range_salary={item.range_salary}
                    />
                  );
                })
              ) : (
                <Loading />
              )}
            </div>
          </div>
          <JobFilter />
        </div>
      </div>
    </section>
  );
}

export default JobList;
