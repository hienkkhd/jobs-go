"use client";
import React, { useEffect, useState } from "react";

type Props = {};

function HowWork({}: Props) {
  const [active, setActive] = useState(0);
  useEffect(() => {
    const timer = setInterval(() => {
      setActive((prev) => prev + 1);
    }, 2000);
    return () => clearInterval(timer);
  }, []);

  return (
    <div className="flex flex-col gap-3 mt-6">
      <div className="flex gap-4 pb-8 relative overflow-hidden">
        <div className="relative z-100 p-1 bg-white w-[50px] h-[50px]">
          <div
            className={`transition-all duration-300 w-[40px] h-[40px] bg-gray-500/20 rounded-full flex items-center justify-center text-lg font-medium text-gray-500 ${
              active % 3 === 0 && "bg-violet-500 text-white scale-[1.7]"
            } `}
          >
            1
          </div>
        </div>
        <div className="absolute w-0 h-full border border-dashed -z-10 left-[25px]"></div>
        <div>
          <h3 className="text-xl font-semibold text-gray-600">
            Register an account
          </h3>
          <p className="mt-1 text-md font-medium text-gray-500">
            Due to its widespread use as filler text for layouts,
            non-readability is of great importance.
          </p>
        </div>
      </div>
      <div className="flex gap-4 pb-8 relative overflow-hidden">
        <div className="relative z-100 p-1 bg-white w-[50px] h-[50px]">
          <div
            className={`transition-all duration-300 w-[40px] h-[40px] bg-gray-500/20 rounded-full flex items-center justify-center text-lg font-medium text-gray-500 ${
              active % 3 === 1 && "bg-violet-500 text-white scale-[1.7]"
            } `}
          >
            2
          </div>
        </div>
        <div className="absolute w-0 h-full border border-dashed -z-10 left-[25px]"></div>
        <div>
          <h3 className="text-xl font-semibold text-gray-600">
            Register an account
          </h3>
          <p className="mt-1 text-md font-medium text-gray-500">
            Due to its widespread use as filler text for layouts,
            non-readability is of great importance.
          </p>
        </div>
      </div>
      <div className="flex gap-4 pb-8 relative overflow-hidden">
        <div className="relative z-100 p-1 bg-white w-[50px] h-[50px]">
          <div
            className={`transition-all duration-300 w-[40px] h-[40px] bg-gray-500/20 rounded-full flex items-center justify-center text-lg font-medium text-gray-500 ${
              active % 3 === 2 && "bg-violet-500 text-white scale-[1.7]"
            } `}
          >
            3
          </div>
        </div>
        <div className="absolute w-0 h-full border border-dashed -z-10 left-[25px]"></div>
        <div>
          <h3 className="text-xl font-semibold text-gray-600">
            Register an account
          </h3>
          <p className="mt-1 text-md font-medium text-gray-500">
            Due to its widespread use as filler text for layouts,
            non-readability is of great importance.
          </p>
        </div>
      </div>
    </div>
  );
}

export default HowWork;
