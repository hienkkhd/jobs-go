import bursts from "@/assets/images/icons-bursts.png";
import { prisma } from "@/lib/prisma";
import { JobCategory } from "@prisma/client";
import Image from "next/image";

async function Categories() {
  const data: JobCategory[] = await prisma.jobCategory.findMany();
  const categories = data.slice(0, 8);

  return (
    <div className="grid grid-cols-12 ">
      {categories.map((item, index) => {
        return (
          <div
            key={index}
            className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 px-6 py-5 lg:py-10 flex items-center justify-center flex-col transition duration-500 hover:-translate-y-3 overflow-hidden cursor-pointer"
          >
            <div className=" bg-violet-500/20 p-5 rounded-xl shadow-md">
              <Image src={bursts} alt="Icon" width={36} height={36} />
            </div>
            <div className="mt-10 text-center">
              <h4 className="text-lg font-semibold text-gray-900">
                {item.category}
              </h4>
              <p className="mt-1 font-medium text-gray-500">2024 Jobs</p>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default Categories;
