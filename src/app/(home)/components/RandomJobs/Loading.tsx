"use client";
import { Avatar, List, Skeleton } from "antd";

type Props = {};

const Loading = (props: Props) => {
  const array = Array(8).fill(null);

  return (
    <div>
      <div className="flex justify-center"></div>
      <div className="mt-14">
        {array.map((item, index) => {
          return (
            <Skeleton key={index} loading active avatar>
              <List.Item.Meta
                avatar={<Avatar />}
                title={"Loading"}
                description={"Loading"}
              />
            </Skeleton>
          );
        })}
      </div>
    </div>
  );
};

export default Loading;
