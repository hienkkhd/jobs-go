"use client";
import { useState, Suspense } from "react";
import JobCard from "@/app/components/JobCard";
import CSSegmented from "@/app/components/Segmented";
import { fetcher } from "@/lib/fetcher";
import { JOB_TYPE, JobCategory, JobPost } from "@prisma/client";
import useSWR from "swr";
import { SegmentedValue } from "antd/es/segmented";
import { Avatar, List, Skeleton } from "antd";
import Loading from "./Loading";
export default async function RandomJobs() {
  const [type, setType] = useState<SegmentedValue>("");
  const { data, isLoading, error } = useSWR(`/api/jobs?type=${type}`, fetcher);
  const listJob: JobPost[] = data?.data;

  return (
    <>
      <div className="flex justify-center">
        <CSSegmented
          value={type}
          onChange={(value: SegmentedValue) => {
            setType(value);
          }}
          options={[
            { value: "", label: "Recent Jobs" },
            { value: JOB_TYPE.FEATURED, label: "Featured Jobs" },
            { value: JOB_TYPE.FREELANCER, label: "Freelancer" },
            { value: JOB_TYPE.PARTTIME, label: "Part Time" },
            { value: JOB_TYPE.FULLTIME, label: "Full Time" },
          ]}
        />
      </div>
      <div className="mt-14">
        {!isLoading ? (
          listJob.map((item, index) => {
            return (
              <JobCard
                title={item.title}
                jobType={item.jobType}
                key={index}
                range_salary={item.range_salary}
              />
            );
          })
        ) : (
          <Loading />
        )}
      </div>
    </>
  );
}
