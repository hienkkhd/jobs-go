"use client";
import { LoginGoogleButton } from "@/app/components/Button";
import signInImage from "@/assets/images/sign-in.png";
import { SignInResponse, signIn } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

export default async function Login({ searchParams }: any) {
  const router = useRouter();
  const submitCredential = async (e: any) => {
    e.preventDefault();
    const data = new FormData(e.target);
    const email = data.get("email");
    const password = data.get("password");
    signIn("credentials", {
      email,
      password,
      callbackUrl: searchParams.callbackUrl || "/",
      redirect: false,
    })
      .then((res: any) => {
        const { error, url } = res;
        if (error) {
          toast.error(
            "Tên đăng nhập hoặc mật khẩu không chính xác. Vui lòng thử lại!"
          );
        } else {
          router.push(url);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <section className="flex justify-center items-center h-[100vh] bg-violet-500/10">
      <div className="container mx-auto">
        <div className="grid grid-cols-12">
          <div className="col-span-12 lg:col-span-8 lg:col-start-3 bg-white grid grid-cols-12 rounded-md overflow-hidden ">
            <div className="p-10 flex justify-center items-center col-span-6 flex-col">
              <Image
                src={"/logo-dark.png"}
                alt="Logo"
                width={125}
                height={27}
              />
              <Image src={signInImage} alt="Sign in" width={444} height={444} />
            </div>
            <div className="bg-violet-600 p-12 flex justify-center items-center col-span-6 flex-col text-white">
              <h1 className="text-xl font-semibold mb-3">Welcome Back !</h1>
              <p className="text-white/80 text-sm">
                Sign in to continue to Jobcy.
              </p>
              <form
                onSubmit={submitCredential}
                className="w-full flex flex-col mt-8"
              >
                <div className="mb-4">
                  <p className="text-md mb-1 font-normal">Email</p>
                  <input
                    type="email"
                    name="email"
                    className="h-[46px] rounded-md p-2 bg-blue-400/40 text-white focus:border-[1px] focus:border-white focus:outline-none w-full"
                  />
                </div>
                <div>
                  <p className="text-md mb-1 font-normal">Password</p>
                  <input
                    type="password"
                    name="password"
                    className="h-[46px] rounded-md p-2 bg-blue-400/40 text-white focus:border-[1px] focus:border-white focus:outline-none w-full"
                  />
                </div>
                <div className="mt-3 flex items-center justify-between">
                  <div className="flex items-center">
                    <input
                      type="checkbox"
                      name="remember"
                      id="remember"
                      title="remember"
                    />
                    <label htmlFor="remember" className="ml-2 text-sm">
                      Remember Me
                    </label>
                  </div>
                  <Link href={"/"} className="text-sm">
                    Forgot Password?
                  </Link>
                </div>
                <button
                  type="submit"
                  className="py-3 bg-white rounded-md text-gray-800 font-medium mt-8"
                >
                  Sign In
                </button>
              </form>
              <p className="text-[16px] mt-5">
                Don{"'"}t have an account ?{" "}
                <Link
                  href={"/register"}
                  className="decoration-slate-100 underline"
                >
                  Sign Up
                </Link>
              </p>
              <p className="mt-4">---- OR ----</p>
              <div className="mt-5">
                <LoginGoogleButton callbackUrl={searchParams?.callbackUrl} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
