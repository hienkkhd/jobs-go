"use client";
import { LoginGoogleButton } from "@/app/components/Button";
import signInImage from "@/assets/images/sign-in.png";
import { registerAction } from "@/lib/action";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

export default async function Register() {
  const router = useRouter();
  return (
    <section className="flex justify-center items-center h-[100vh] bg-violet-500/10">
      <div className="container mx-auto">
        <div className="grid grid-cols-12">
          <div className="col-span-12 lg:col-span-8 lg:col-start-3 bg-white grid grid-cols-12 rounded-md overflow-hidden ">
            <div className="p-10  justify-center items-center hidden lg:flex col-span-0 lg:col-span-6 flex-col">
              <Image
                src={"/logo-dark.png"}
                alt="Logo"
                width={125}
                height={27}
              />
              <Image src={signInImage} alt="Sign in" width={444} height={444} />
            </div>
            <div className="bg-violet-600 p-12 flex justify-center items-center col-span-12 lg:col-span-6 flex-col text-white">
              <h1 className="text-xl font-semibold mb-3">
                Let{"'"}s Get Started
              </h1>
              <p className="text-white/80 text-sm">
                Sign Up and get access to all the features of Jobcy
              </p>
              <form
                action={async (formData) => {
                  const res = await registerAction(formData);
                  if (res.success) {
                    toast.success(res.message);
                    router.push("/login");
                  } else {
                    toast.error(res.message);
                  }
                }}
                className="w-full flex flex-col mt-8"
              >
                <div className="mb-4">
                  <p className="text-md mb-1 font-normal">Your name</p>
                  <input
                    type="text"
                    name="name"
                    className="h-[46px] rounded-md p-2 bg-blue-400/40 text-white focus:border-[1px] focus:border-white focus:outline-none w-full"
                    required
                  />
                </div>
                <div className="mb-4">
                  <p className="text-md mb-1 font-normal">Email</p>
                  <input
                    type="email"
                    name="email"
                    className="h-[46px] rounded-md p-2 bg-blue-400/40 text-white focus:border-[1px] focus:border-white focus:outline-none w-full"
                    required
                  />
                </div>
                <div className="mb-4">
                  <p className="text-md mb-1 font-normal">Password</p>
                  <input
                    type="password"
                    name="password"
                    min={8}
                    className="h-[46px] rounded-md p-2 bg-blue-400/40 text-white focus:border-[1px] focus:border-white focus:outline-none w-full"
                    required
                  />
                </div>
                <div>
                  <p className="text-md mb-1 font-normal">Confirm password</p>
                  <input
                    type="password"
                    name="rePassword"
                    min={8}
                    className="h-[46px] rounded-md p-2 bg-blue-400/40 text-white focus:border-[1px] focus:border-white focus:outline-none w-full"
                    required
                  />
                </div>
                <div className="mt-3 flex items-center justify-between">
                  <div className="flex items-center">
                    <input
                      type="checkbox"
                      name="agree"
                      id="agree"
                      title="agree"
                    />
                    <label htmlFor="agree" className="ml-2 text-sm">
                      I agree to the Terms and conditions
                    </label>
                  </div>
                </div>
                <button
                  type="submit"
                  className="py-3 bg-white rounded-md text-gray-800 font-medium mt-8"
                >
                  Sign Up
                </button>
              </form>
              <p className="text-[16px] mt-5">
                Already a member ?
                <Link
                  href={"/login"}
                  className="decoration-slate-100 underline"
                >
                  Sign In
                </Link>
              </p>
              <p className="mt-4">---- OR ----</p>
              <div className="mt-5">
                <LoginGoogleButton />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
