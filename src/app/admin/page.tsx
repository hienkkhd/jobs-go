import { authOptions } from "@/lib/auth";
import { getServerSession } from "next-auth";

type Props = {};

async function Page({}: Props) {
  const session = await getServerSession(authOptions);
  console.log(session);

  return <div>Page</div>;
}

export default Page;
