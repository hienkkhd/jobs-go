import { authOptions } from "@/lib/auth";
import { legacyLogicalPropertiesTransformer } from "@ant-design/cssinjs";
import { getServerSession } from "next-auth";
import { NextResponse } from "next/server";
export const dynamic = "force-dynamic";

export async function GET() {
  const user = await getServerSession(authOptions);
  if (!user) {
    return NextResponse.json(
      {
        message: "User is not authenticated",
        user: null,
      },
      {
        status: 403,
      }
    );
  }

  return NextResponse.json(user);
}
