import { prisma } from "@/lib/prisma";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  
  const id = searchParams.get("id");
  if (id) {
    const res = await prisma.jobCategory.findFirst({
      where: {
        id,
      },
    });
    return NextResponse.json({ data: res });
  }
  const res = await prisma.jobCategory.findMany();
  return NextResponse.json({ data: res });
}
