import { prisma } from "@/lib/prisma";
import { BusinessStream, Company } from "@prisma/client";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const page = Number(searchParams.get("page")) || 1;
  const limit = Number(searchParams.get("limit")) || 10;
  if (page < 1 || limit < 1) {
    return NextResponse.json(
      {
        data: null,
        message: "Invalid params",
      },
      {
        status: 404,
      }
    );
  }
  const total = await prisma.company.count();
  const businessStream = await prisma.company.findMany({
    include: {
      _count: {
        select: {
          JobPost: true,
        },
      },
      Company_BusinessStream: {
        select: {
          businessStream: {
            select: {
              stream_name_en: true,
              stream_name_vi: true,
            },
          },
        },
      },
      companyImage: {
        select: {
          image_url: true,
        },
      },
    },
    skip: (page - 1) * limit,
    take: limit,
  });
  return NextResponse.json({
    data: businessStream,
    meta: {
      page: page,
      limit: limit,
      total: total,
    },
  });
}
