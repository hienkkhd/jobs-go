import { prisma } from "@/lib/prisma";
import { JOB_TYPE } from "@prisma/client";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const page = Number(searchParams.get("page")) || 1;
  const limit = Number(searchParams.get("limit")) || 10;
  const type = <JOB_TYPE>searchParams.get("type");
  const total = await prisma.jobPost.count();
  if (page < 1 || limit < 1) {
    return NextResponse.json(
      {
        data: null,
        message: "Invalid params",
      },
      {
        status: 404,
      }
    );
  }
  const res = await prisma.jobPost.findMany({
    where: type
      ? {
          jobType: type,
        }
      : {},
    skip: (page - 1) * limit,
    take: limit,
  });
  return NextResponse.json({
    data: res,
    meta: {
      page: page,
      limit: limit,
      type,
      total: total,
    },
  });
}
