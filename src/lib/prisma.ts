import { PrismaClient } from "@prisma/client";
import slugify from "slugify";

const globalForPrisma = global as unknown as { prisma: PrismaClient };

export const prisma = globalForPrisma.prisma || new PrismaClient({});
prisma.$use(async (params, next) => {
  if (
    (params.action === "create" || params.action === "update") &&
    ["Blog"].includes(params.model as string)
  ) {
    let {
      args: { data },
    } = params;
    // Check if slug exists by `findUnique` (did not test)
    data.slug = slugify(`${data.name ? data.name : data.title}`, {
      replacement: "-",
      lower: true,
      strict: true,
      remove: /[*+~.()'"!:@]/g,
    });
  }
  const result = await next(params);
  return result;
});
if (process.env.NODE_ENV !== "production") globalForPrisma.prisma = prisma;
