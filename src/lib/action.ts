"use server";

import { hash } from "bcrypt";
import { prisma } from "./prisma";

export const registerAction = async (data: FormData) => {
  const email = data.get("email") as string;
  const name = data.get("name") as string;
  const password = await hash(data.get("password") as string, 10);
  try {
    if (!email) {
      return {
        success: false,
        message: "Email is required",
        code: "EMAIL_REQUIRED",
      };
    }
    const checkUser = await prisma.user.findFirst({
      where: {
        email,
      },
    });
    if (checkUser) {
      return {
        success: false,
        message:
          "Email already exists. Please try again with diferent email or login!",
        code: "USER_EXISTS",
      };
    }
    const res = await prisma.user.create({
      data: {
        email,
        password,
        name,
      },
    });
    if (res) {
      return {
        success: true,
        message: "Resgister successfully.",
        user: res,
      };
    }
    return {
      success: false,
      message: "Something went wrong.Please try again!",
      code: "BAD_REQUEST",
    };
  } catch (error) {
    console.log(error);
    return {
      success: false,
      message: "Something went wrong.Please try again!",
      code: "BAD_REQUEST",
    };
  }
};
