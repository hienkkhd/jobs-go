export function registerValidate(action: any) {
  return async (formData: FormData) => {
    "use server";
    return action(formData);
  };
}
