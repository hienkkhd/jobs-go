"use client";
import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
type Props = {};

function ToastifyContainer({}: Props) {
  return <ToastContainer />;
}

export default ToastifyContainer;
